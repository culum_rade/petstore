﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PetStore.Exceptions
{
    public class PurchaseValidationException : Exception
    {
        public PurchaseValidationException()
        {
        }

        public PurchaseValidationException(string message) : base(message)
        {
        }
    }
}
