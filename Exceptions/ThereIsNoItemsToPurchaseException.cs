﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PetStore.Exceptions
{
    public class ThereIsNoItemsToPurchaseException : PurchaseValidationException
    {
        public ThereIsNoItemsToPurchaseException()
        {
        }

        public ThereIsNoItemsToPurchaseException(string message) : base(message)
        {
        }
    }
}
