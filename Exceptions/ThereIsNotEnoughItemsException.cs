﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PetStore.Exceptions
{
    public class ThereIsNotEnoughItemsException : PurchaseValidationException
    {
        public ThereIsNotEnoughItemsException()
        {
        }

        public ThereIsNotEnoughItemsException(string message) : base(message)
        {
        }
    }
}
