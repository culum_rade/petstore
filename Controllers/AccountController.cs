﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using PetStore.Models.ViewModels.Account;
using PetStore.Models.ViewModels.Error;
using PetStore.Services.ActionServices.Account.Interfaces;
using PetStore.Services.ActionServices.ErrorActionServices.Interfaces;

namespace PetStore.Controllers
{
    public class AccountController : Controller
    {
        private readonly SignInManager<IdentityUser> signInManager;
        private readonly IErrorBaseService errorService;

        public AccountController(
            SignInManager<IdentityUser> signInManager,
            IErrorBaseService errorService
            )
        {
            this.signInManager = signInManager;
            this.errorService = errorService;
        }

        [HttpGet]
        public IActionResult Register()
        {
            if (signInManager.IsSignedIn(User))
            {
                return Redirect("/Home");
            }
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Register([FromServices] IAccountRegisterService registerService, AccountRegisterViewModel viewModel)
        {
            if (signInManager.IsSignedIn(User))
            {
                return Redirect("/Home");
            }

            if (ModelState.IsValid)
            {
                bool success = await registerService.Handle(viewModel);

                if (success)
                {
                    return RedirectToAction("index", "home");
                }

                ErrorViewModel errorViewModel = errorService.Handle(500, "Registration Failed");

                return View("Error", errorViewModel);
            }

            return View(viewModel);
        }

        [HttpGet]
        public ViewResult Login()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Login([FromServices] IAccountLoginService loginService, AccountLoginViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                bool loginSucceeded = await loginService.Handle(viewModel);

                if (loginSucceeded)
                {
                    return Redirect("/Home");
                }

                ModelState.AddModelError(string.Empty, "Invalid Login Atempt");
            }

            return View(viewModel);
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> Logout([FromServices] IAccountLogoutService logoutService)
        {
            await logoutService.Handle();

            return Redirect("/Home");
        }
    }
}
