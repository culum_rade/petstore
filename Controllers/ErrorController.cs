﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PetStore.Models;
using PetStore.Models.ViewModels.Error;

namespace PetStore.Controllers
{
    public class ErrorController : Controller
    {
        private readonly ILogger logger;

        public ErrorController(ILogger<ErrorController> logger)
        {
            this.logger = logger;
        }

        [Route("Error/{statusCode}")]
        public IActionResult HttpStatusCodeHandler(int statusCode)
        {
            IStatusCodeReExecuteFeature statusCodeResult = HttpContext.Features.Get<IStatusCodeReExecuteFeature>();
            string message;

            switch (statusCode)
            {
                case 400:
                    message = "Bad request!";
                    break;
                case 404:
                    message = "Sorry, the resource you requested colud not be found!";
                    break;
                default:
                    message = "Error occured!";
                    break;
            }
            logger.LogWarning($"{statusCode} Error occured. Path = {statusCodeResult.OriginalPath}" +
                       $"and QuryString = {statusCodeResult.OriginalQueryString}");

            ErrorViewModel viewModel = new ErrorViewModel
            {
                StatusCode = statusCode,
                Message = message
            };
            return View("Error", viewModel);
        }

        [Route("Error")]
        [AllowAnonymous]
        public IActionResult Error()
        {
            IExceptionHandlerPathFeature exceptionDetails = HttpContext.Features.Get<IExceptionHandlerPathFeature>();

            ErrorViewModel viewModel = new ErrorViewModel()
            {
                StatusCode = 500,
                Message = "Error occured"
            };

            logger.LogError($"The path {exceptionDetails.Path} threw an exception {exceptionDetails.Error}");

            return View("Error", viewModel);
        }
    }
}
