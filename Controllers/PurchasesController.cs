﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using PetStore.Exceptions;
using PetStore.Models.ViewModels.Purchases;
using PetStore.Services.ActionServices.PurchasesActionServices.Interfaces;

namespace PetStore.Controllers
{
    public class PurchasesController : Controller
    {
        [Authorize]
        [HttpGet]
        public async Task<ViewResult> Index([FromServices] IPurchasesIndexService indexService, int? page)
        {
            PurchasesIndexViewModel viewModel = await indexService.Handle(page);

            return View(viewModel);
        }

        [HttpGet]
        public ViewResult Create([FromServices] IPurchasesGetCreateService getCreateService, string cartItemsIdsQueryString)
        {
            PurchasesCreateViewModel viewModel = getCreateService.Handle(cartItemsIdsQueryString);

            return View(viewModel);
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromServices] IPurchasePostCreateService postCreateService, PurchasesCreateViewModel viewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    await postCreateService.Handle(viewModel);

                    return Redirect("Confirmation");
                }

                return View(viewModel);
            }
            catch (PurchaseValidationException)
            {
                return BadRequest();
            }
        }

        public ViewResult Confirmation()
        {
            return View("Confirmation");
        }
    }
}
