﻿using System;
using System.Collections;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using PetStore.Exceptions;
using PetStore.Models.ViewModels.Toys;
using PetStore.Services.ActionServices.ToysActionServices.Interfaces;
using PetStore.Services.ViewModelServices.ToysViewModelServices.Interfaces;

namespace PetStore.Controllers
{
    public class ToysController : Controller
    {
        [HttpGet]
        public ViewResult Index([FromServices] IToysIndexService indexService, string searchString, int? page)
        {
            return View(indexService.Handle(User, searchString, page));
        }

        [HttpGet]
        public async Task<ActionResult> Details([FromServices] IToysDetailsService detailsService, int id)
        {
            try
            {
                ToysDetailsViewModel viewModel = await detailsService.Handle(User, id);

                return View(viewModel);
            }
            catch (NotFoundException)
            {
                return NotFound();
            }
        }

        [Authorize]
        [HttpGet]
        public ViewResult Create()
        {
            return View();
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> Create([FromServices] IToysCreateService createService, ToysCreateViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                await createService.Handle(viewModel);
            }
            return RedirectToAction("Index");
        }

        [Authorize]
        [HttpGet]
        public async Task<IActionResult> Edit([FromServices] IToysGetEditService getEditService, int id)
        {
            try
            {
                ToysEditViewModel viewModel = await getEditService.Handle(id);

                return View(viewModel);
            }
            catch (NotFoundException)
            {
                return NotFound();
            }

        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> Edit([FromServices] IToysPostEditService postEditService, ToysEditViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    await postEditService.Handle(viewModel);
                }
                catch (NotFoundException)
                {
                    return NotFound();
                }

                return RedirectToAction("Index");
            }

            return View(viewModel);
        }

        [Authorize]
        [HttpGet]
        public async Task<IActionResult> Delete([FromServices] IToyDeleteService deleteService, int id)
        {
            try
            {
                await deleteService.Handle(id);

                return Redirect("/Home");
            }
            catch (NotFoundException)
            {
                return NotFound();
            }

        }
    }
}
