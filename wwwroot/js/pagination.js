﻿$(document).ready(function () {
    var currentPage = parseInt($("#index-pagination-input").val());
    var maxPage = parseInt($("#index-pagination-input").attr('max'));

    if (currentPage === 1) {
        $("#index-pagination-prev").attr("disabled", "disabled");
    };

    if (currentPage >= maxPage) {
        $("#index-pagination-next").attr("disabled", "disabled");
    }

    $("#index-pagination-next").on("click", () => {
        $("#index-pagination-input").val(currentPage + 1);

        submitSearcPaginationForm();
    });

    $("#index-pagination-prev").on("click", () => {
        $("#index-pagination-input").val(currentPage - 1);

        submitSearcPaginationForm();
    });

    function submitSearcPaginationForm() {
        $("#pagination-form-btn").trigger("click");
    }
});