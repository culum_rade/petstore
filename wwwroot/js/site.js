﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.

$(document).ready(function () {
    var cart = $.cookie('CART')? JSON.parse($.cookie('CART')) : [];

    $('.cart-item').click(function () {
        let itemId = $(this).attr('data');

        if ($(this).is(':checked')) {
            cart.push(itemId);
        } else {
            var index = cart.indexOf(itemId);
            cart.splice(index, 1);
        }
        cart.sort();

        $("#cart-items-count").html(cart.length);

        $.cookie('CART', JSON.stringify(cart), { path:'/', expires: 3600 });
        $("#cart-items-ids").val(cart.toString());
    });

    $("#cart-link").on("click", function (e) {
        e.preventDefault();
        $("#go-to-cart-btn").trigger('click');
    });


    loadCartFromCookie();

    function loadCartFromCookie() {
        for (let itemId of cart) {
            let item = $("#cart-item-" + itemId);
            if (item) {
                $(item).attr('checked', 'checked');
            }
        }
        $("#cart-items-count").html(cart.length);
        cart_str = JSON.stringify(cart);
        $("#cart-items-ids").val(cart.toString());
    };

    function clearCartCookie() {
        $.removeCookie('CART', { path: '/' });
        $("#cart-items-count").html(0);
    }
});
