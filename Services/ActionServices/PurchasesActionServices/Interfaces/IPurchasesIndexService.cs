﻿using PetStore.DataAccess.Interfaces;
using PetStore.Models.ViewModels.Purchases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PetStore.Services.ActionServices.PurchasesActionServices.Interfaces
{
    public interface IPurchasesIndexService
    {
        Task<PurchasesIndexViewModel> Handle(int? page);
    }
}
