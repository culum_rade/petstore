﻿using PetStore.Models.ViewModels;
using PetStore.Models.ViewModels.Purchases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PetStore.Services.ActionServices.PurchasesActionServices.Interfaces
{
    public interface IPurchasePostCreateService
    {
        public Task Handle(PurchasesCreateViewModel viewModel);
    }
}
