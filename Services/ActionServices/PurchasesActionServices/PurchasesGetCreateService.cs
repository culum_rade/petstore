﻿using PetStore.DataAccess.Interfaces;
using PetStore.Helpers;
using PetStore.Models.DomainModels;
using PetStore.Models.ViewModels;
using PetStore.Models.ViewModels.Purchases;
using PetStore.Services.ActionServices.PurchasesActionServices.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PetStore.Services.ActionServices.PurchasesActionServices
{
    public class PurchasesGetCreateService : IPurchasesGetCreateService
    {
        private readonly IPurchaseRepository purchaseRepository;
        private readonly IItemRepositor<Toy> toyRepository;

        public PurchasesGetCreateService(IPurchaseRepository purchaseRepository, IItemRepositor<Toy> toyRepository)
        {
            this.purchaseRepository = purchaseRepository;
            this.toyRepository = toyRepository;
        }

        public PurchasesCreateViewModel Handle(string queryString)
        {
            List<int> cartItemsIdsList = ParserHelper.Instance.TextSeparatedByDelimiterToList(queryString, ',');

            PurchasesCreateViewModel viewModel = new PurchasesCreateViewModel();
            viewModel.Toys = toyRepository.GetByIds(cartItemsIdsList).ToList();
            viewModel.PurchesCartItems =  Enumerable.Repeat<PurchaseCartItem>(new PurchaseCartItem(), viewModel.Toys.Count()).ToList();

            return viewModel;
        }
    }
}
