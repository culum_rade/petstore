﻿using Microsoft.Extensions.Configuration;
using PetStore.DataAccess.Interfaces;
using PetStore.Models.ViewModels.Purchases;
using PetStore.Services.ActionServices.PurchasesActionServices.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PetStore.Services.ActionServices.PurchasesActionServices
{
    public class PurchasesIndexService : IPurchasesIndexService
    {
        private readonly IPurchaseRepository purchaseRepository;
        private readonly IConfiguration configuration;

        public PurchasesIndexService(IPurchaseRepository purchaseRepository, IConfiguration configuration)
        {
            this.purchaseRepository = purchaseRepository;
            this.configuration = configuration;
        }

        public async Task<PurchasesIndexViewModel> Handle(int? page)
        {
            int pageSize = int.Parse(configuration["Pagination:PurchasesIndexItemsPerPage"]);
            int totalPurcasesCount = await purchaseRepository.Count();

            PurchasesIndexViewModel viewModel = new PurchasesIndexViewModel();

            viewModel.MaxPage = (int)Math.Ceiling(totalPurcasesCount / (float)pageSize);
            viewModel.CurrentPage = page ?? 1;
            viewModel.Purchases = purchaseRepository.Paginated(pageSize, viewModel.CurrentPage);

            return viewModel;
        }
    }
}
