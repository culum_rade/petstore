﻿using Microsoft.EntityFrameworkCore.Storage;
using PetStore.DataAccess.Interfaces;
using PetStore.Exceptions;
using PetStore.Helpers;
using PetStore.Model;
using PetStore.Models.DomainModels;
using PetStore.Models.ViewModels.Purchases;
using PetStore.Services.ActionServices.PurchasesActionServices.Interfaces;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;

namespace PetStore.Services.ActionServices.PurchasesActionServices
{
    public class PurchasePostCreateService : IPurchasePostCreateService
    {
        private readonly IPurchaseRepository purchaseRepository;
        private readonly IItemRepositor<Toy> toyRepository;
        private readonly IItemPurchaseRepository itemPurchaseRepository;
        private readonly PetStoreDbContext context;

        public PurchasePostCreateService(
            IPurchaseRepository purchaseRepository,
            IItemRepositor<Toy> toyRepository,
            IItemPurchaseRepository itemPurchaseRepository,
            PetStoreDbContext context)
        {
            this.purchaseRepository = purchaseRepository;
            this.toyRepository = toyRepository;
            this.itemPurchaseRepository = itemPurchaseRepository;
            this.context = context;
        }

        public async Task Handle(PurchasesCreateViewModel viewModel)
        {
            int selectedItemsCount = viewModel.PurchesCartItems.Where(pt => pt.IsChecked).Count();

            if (selectedItemsCount == 0)
            {
                throw new ThereIsNoItemsToPurchaseException("There is no items to purchase.Cart is empty.");
            }

            using (IDbContextTransaction contextTransaction = context.Database.BeginTransaction())
            {
                Purchase purchase = new Purchase
                {
                    PurchaserFirstName = viewModel.PurchaserFirstName,
                    PurchaserLatsName = viewModel.PurchaserLatsName,
                    PurchaserAddress = viewModel.PurchaserAddress,
                    PurchaserCardNumber = CryptoHelper.Instance.ComputeMD5Hash(viewModel.PurchaserCardNumber),
                    ItemPurchases = new Collection<ItemPurchase>()
                };

                purchase = await purchaseRepository.Insert(purchase);

                foreach (PurchaseCartItem toyPurchase in viewModel.PurchesCartItems.Where(pct => pct.IsChecked))
                {
                    await toyRepository.DecreaseStock(toyPurchase.ItemId, toyPurchase.Quantity);

                    ItemPurchase itemPurchase = new ItemPurchase()
                    {
                        PurchaseId = purchase.Id,
                        ItemId = toyPurchase.ItemId,
                        Quantity = toyPurchase.Quantity,

                    };

                    await itemPurchaseRepository.Insert(itemPurchase);
                }
                contextTransaction.Commit();
            }
        }
    }
}
