﻿using Microsoft.Extensions.DependencyInjection;
using PetStore.Services.ActionServices.Account;
using PetStore.Services.ActionServices.Account.Interfaces;
using PetStore.Services.ActionServices.ErrorActionServices;
using PetStore.Services.ActionServices.ErrorActionServices.Interfaces;
using PetStore.Services.ActionServices.PurchasesActionServices;
using PetStore.Services.ActionServices.PurchasesActionServices.Interfaces;
using PetStore.Services.ActionServices.ToysActionServices;
using PetStore.Services.ActionServices.ToysActionServices.Interfaces;
using PetStore.Services.ViewModelServices.ToysViewModelServices.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PetStore.Services
{
    public static class ActionSerivcesInjector
    {
        public static void AddActionServices(this IServiceCollection services)
        {
            services.AddScoped<IToysIndexService, ToysIndexService>();
            services.AddScoped<IToysDetailsService, ToysDetailsService>();
            services.AddScoped<IErrorBaseService, ErrorBaseService>();
            services.AddScoped<IToysCreateService, ToysCreateService>();
            services.AddScoped<IToysGetEditService, ToysGetEditService>();
            services.AddScoped<IToysPostEditService, ToysPostEditService>();
            services.AddScoped<IToyDeleteService, ToyDeleteService>();
            services.AddScoped<IPurchasesIndexService, PurchasesIndexService>();
            services.AddScoped<IPurchasesGetCreateService, PurchasesGetCreateService>(); 
            services.AddScoped<IPurchasePostCreateService, PurchasePostCreateService>(); 
            services.AddScoped<IAccountRegisterService, AccountRegisterService>(); 
            services.AddScoped<IAccountLoginService, AccountLoginService>(); 
            services.AddScoped<IAccountLogoutService, AccountLogoutService>(); 
        }
    }
}
