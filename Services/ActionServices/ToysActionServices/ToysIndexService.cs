﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using PetStore.DataAccess.Interfaces;
using PetStore.Helpers;
using PetStore.Model;
using PetStore.Models.DomainModels;
using PetStore.Models.ViewModels.Toys;
using PetStore.Services.ViewModelServices.ToysViewModelServices.Interfaces;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace PetStore.Services.ActionServices.ToysActionServices
{
    public class ToysIndexService : IToysIndexService
    {
        private readonly IItemRepositor<Toy> toyRepository;
        private readonly SignInManager<IdentityUser> signInManager;
        private readonly IConfiguration configuration;

        public ToysIndexService(IItemRepositor<Toy> toyRepository, SignInManager<IdentityUser> signInManager, IConfiguration configuration)
        {
            this.toyRepository = toyRepository;
            this.signInManager = signInManager;
            this.configuration = configuration;
        }
        public ToysIndexViewModel Handle(ClaimsPrincipal currentUser, string searchString, int? page)
        {
            ToysIndexViewModel viewModel = new ToysIndexViewModel();
            IEnumerable<Toy> nonPagedToys = toyRepository.All();

            viewModel.TotalToysCount = nonPagedToys.Count();
            viewModel.ItemsPerPage = int.Parse(configuration["Pagination:PetToysIndexItemsPerPage"]);
            viewModel.CurrentPageNumber = page ?? 1;
            viewModel.SearchString = searchString;

            if (!signInManager.IsSignedIn(currentUser))
            {
                nonPagedToys = toyRepository.AvilableInStock().ToList();
            }

            if (!String.IsNullOrEmpty(searchString))
            {
                nonPagedToys = toyRepository.SearchByName(nonPagedToys, searchString).ToList();
            }

            viewModel.CurrentPageToys = toyRepository.GetPage(nonPagedToys, viewModel.ItemsPerPage, viewModel.CurrentPageNumber);

            return viewModel;
        }

    }
}
