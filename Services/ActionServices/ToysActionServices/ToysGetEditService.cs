﻿using PetStore.DataAccess.Interfaces;
using PetStore.Exceptions;
using PetStore.Models.DomainModels;
using PetStore.Models.ViewModels.Toys;
using PetStore.Services.ActionServices.ToysActionServices.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PetStore.Services.ActionServices.ToysActionServices
{
    public class ToysGetEditService : IToysGetEditService
    {
        private readonly IItemRepositor<Toy> toyRepository;

        public ToysGetEditService(IItemRepositor<Toy> toyRepository)
        {
            this.toyRepository = toyRepository;
        }
        public async Task<ToysEditViewModel> Handle(int id)
        {
            Toy toy = await toyRepository.Get(id);

            if(toy == null)
            {
                throw new NotFoundException();
            }

            ToysEditViewModel viewModel = new ToysEditViewModel
            {
                Id = toy.Id,
                Name = toy.Name,
                Description = toy.Description,
                ItemUnit = toy.ItemUnit,
                InStock = toy.InStock,
                Price = toy.CurrentPrice.Amount
            };

            return viewModel;
        }
    }
}
