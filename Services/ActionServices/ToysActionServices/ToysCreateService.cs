﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using PetStore.DataAccess.Interfaces;
using PetStore.Helpers;
using PetStore.Helpers.Interfaces;
using PetStore.Models.DomainModels;
using PetStore.Models.ViewModels.Toys;
using PetStore.Services.ActionServices.ToysActionServices.Interfaces;
using PetStore.Web.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace PetStore.Services.ActionServices.ToysActionServices
{
    public class ToysCreateService : IToysCreateService
    {
        private readonly IItemRepositor<Toy> toyRepository;
        private readonly IWebHostEnvironment environment;
        private readonly IFileHelper fileHelper;

        public ToysCreateService(
            IItemRepositor<Toy> petToyRepository, 
            IWebHostEnvironment environment,
            IFileHelper fileHelper
            )
        {
            this.toyRepository = petToyRepository;
            this.environment = environment;
            this.fileHelper = fileHelper;
        }
        public async Task Handle(ToysCreateViewModel viewModel)
        {
            if (viewModel.ImageFile != null)
            {
                string imagesDirectoryPath = Path.Combine(environment.WebRootPath, "images/items");
                string imageFilePath = Path.Combine(imagesDirectoryPath, viewModel.ImageFile.FileName);

                await fileHelper.UploadFile(viewModel.ImageFile, imageFilePath);
            }

            Toy toy = new Toy
            {
                Name = viewModel.Name,
                Description = viewModel.Description,
                ItemUnit = viewModel.ItemUnit,
                InStock = viewModel.InStock,
                Image = viewModel.ImageFile?.FileName
            };

            toy.Prices.Add(new Price() { Amount = viewModel.Price });

            await toyRepository.Insert(toy);
        }
    }
}
