﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using PetStore.DataAccess.Interfaces;
using PetStore.Exceptions;
using PetStore.Models.DomainModels;
using PetStore.Models.ViewModels.Toys;
using PetStore.Services.ActionServices.ToysActionServices.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace PetStore.Services.ActionServices.ToysActionServices
{
    public class ToysDetailsService : IToysDetailsService
    {
        private readonly IItemRepositor<Toy> toyRepository;

        public ToysDetailsService(IItemRepositor<Toy> toyRepository)
        {
            this.toyRepository = toyRepository;
        }
        public async Task<ToysDetailsViewModel> Handle(ClaimsPrincipal currentUser, int toyId)
        {
            ToysDetailsViewModel viewModel = new ToysDetailsViewModel
            {
                Toy = await toyRepository.Get(toyId)
            };

            if (viewModel.Toy == null)
            {
                throw new NotFoundException();
            }

            return viewModel;
        }
    }
}
