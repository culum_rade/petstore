﻿using Microsoft.AspNetCore.Hosting;
using PetStore.DataAccess.Interfaces;
using PetStore.Exceptions;
using PetStore.Helpers.Interfaces;
using PetStore.Models.DomainModels;
using PetStore.Models.ViewModels.Toys;
using PetStore.Services.ActionServices.ToysActionServices.Interfaces;
using PetStore.Web.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace PetStore.Services.ActionServices.ToysActionServices
{
    public class ToysPostEditService : IToysPostEditService
    {
        private readonly IItemRepositor<Toy> toyRepository;
        private readonly IWebHostEnvironment environment;
        private readonly IFileHelper fileHelper;

        public ToysPostEditService(
            IItemRepositor<Toy> toyRepository, 
            IWebHostEnvironment environment,
            IFileHelper fileHelper
            )
        {
            this.toyRepository = toyRepository;
            this.environment = environment;
            this.fileHelper = fileHelper;
        }

        public async Task Handle(ToysEditViewModel viewModel)
        {
            Toy toy = await toyRepository.Get(viewModel.Id);

            if(toy == null)
            {
                throw new NotFoundException();
            }

            toy.Name = viewModel.Name;
            toy.Description = viewModel.Description;
            toy.ItemUnit = viewModel.ItemUnit;
            toy.InStock = viewModel.InStock;

            if(viewModel.ImageFile != null && viewModel.ImageFile.FileName != toy.Image)
            {
                toy.Image = viewModel.ImageFile.FileName;

                string imagesDirectoryPath = Path.Combine(environment.WebRootPath, "images/items");
                string imageFilePath = Path.Combine(imagesDirectoryPath, viewModel.ImageFile.FileName);

                await fileHelper.UploadFile(viewModel.ImageFile, imageFilePath);

            }

            if(viewModel.Price != toy.CurrentPrice.Amount)
            {
                toy.Prices.Add( new Price { Amount = viewModel.Price});
            }

            await toyRepository.Update(toy);
        }
    }
}
