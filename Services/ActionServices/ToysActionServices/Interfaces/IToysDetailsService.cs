﻿using PetStore.Models.ViewModels.Toys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace PetStore.Services.ActionServices.ToysActionServices.Interfaces
{
    public interface IToysDetailsService
    {
        public Task<ToysDetailsViewModel> Handle(ClaimsPrincipal currentUser, int toyId);
    }
}
