﻿using PetStore.DataAccess.Interfaces;
using PetStore.Exceptions;
using PetStore.Models.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PetStore.Services.ActionServices.ToysActionServices.Interfaces
{
    public class ToyDeleteService : IToyDeleteService
    {
        private readonly IItemRepositor<Toy> toyRepository;

        public ToyDeleteService(IItemRepositor<Toy> toyRepository)
        {
            this.toyRepository = toyRepository;
        }

        public async Task Handle(int id)
        {
            var toy = await toyRepository.Get(id);
            if (toy == null)
            {
                throw new NotFoundException();
            }

            await toyRepository.SoftDelete(id);
        }
    }
}
