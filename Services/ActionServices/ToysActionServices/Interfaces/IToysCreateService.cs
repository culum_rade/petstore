﻿using PetStore.Models.ViewModels.Toys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PetStore.Services.ActionServices.ToysActionServices.Interfaces
{
    public interface IToysCreateService
    {
        public Task Handle(ToysCreateViewModel viewModel);
    }
}
