﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PetStore.Services.ActionServices.ToysActionServices.Interfaces
{
    public interface IToyDeleteService
    {
        Task Handle(int id);
    }
}
