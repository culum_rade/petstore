﻿using PetStore.Models.ViewModels.Toys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace PetStore.Services.ViewModelServices.ToysViewModelServices.Interfaces
{
    public interface IToysIndexService
    {
        public ToysIndexViewModel Handle(ClaimsPrincipal currentUser, string searchString, int? page);
    }
}
