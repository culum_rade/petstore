﻿using PetStore.Models.ViewModels.Toys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PetStore.Services.ActionServices.ToysActionServices.Interfaces
{
    public interface IToysGetEditService
    {
        public Task<ToysEditViewModel> Handle(int id);
    }
}
