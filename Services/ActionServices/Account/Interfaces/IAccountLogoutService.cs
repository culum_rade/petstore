﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PetStore.Services.ActionServices.Account.Interfaces
{
    public interface IAccountLogoutService
    {
        public Task Handle();
    }
}
