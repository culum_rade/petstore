﻿using PetStore.Models.ViewModels.Account;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PetStore.Services.ActionServices.Account.Interfaces
{
    public interface IAccountRegisterService
    {
        public Task<bool> Handle(AccountRegisterViewModel model);
    }
}
