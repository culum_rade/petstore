﻿using Microsoft.AspNetCore.Identity;
using PetStore.Services.ActionServices.Account.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PetStore.Services.ActionServices.Account
{
    public class AccountLogoutService : IAccountLogoutService
    {
        private readonly SignInManager<IdentityUser> signInManager;

        public AccountLogoutService(SignInManager<IdentityUser> signInManager)
        {
            this.signInManager = signInManager;
        }

        public async Task Handle()
        {
            await signInManager.SignOutAsync();
        }
    }
}
