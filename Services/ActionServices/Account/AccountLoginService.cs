﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using PetStore.Models.ViewModels.Account;
using PetStore.Services.ActionServices.Account.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SignInResult = Microsoft.AspNetCore.Identity.SignInResult;

namespace PetStore.Services.ActionServices.Account
{
    public class AccountLoginService : IAccountLoginService
    {
        private readonly SignInManager<IdentityUser> signInManager;

        public AccountLoginService(SignInManager<IdentityUser> signInManager)
        {
            this.signInManager = signInManager;
        }
        public async Task<bool> Handle(AccountLoginViewModel viewModel)
        {
            SignInResult signInResult = await signInManager
                .PasswordSignInAsync(viewModel.Email, viewModel.Password, viewModel.RememberMe, false);

            return signInResult.Succeeded;
        }
    }
}
