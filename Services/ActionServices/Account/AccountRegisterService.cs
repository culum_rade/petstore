﻿using Microsoft.AspNetCore.Identity;
using PetStore.Models.ViewModels.Account;
using PetStore.Services.ActionServices.Account.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PetStore.Services.ActionServices.Account
{
    public class AccountRegisterService : IAccountRegisterService
    {
        private readonly UserManager<IdentityUser> userManager;
        private readonly SignInManager<IdentityUser> signInManager;

        public AccountRegisterService(UserManager<IdentityUser> userManager, SignInManager<IdentityUser> signInManager)
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
        }

        public async Task<bool> Handle(AccountRegisterViewModel model)
        {
            IdentityUser user = new IdentityUser
            {
                UserName = model.Email,
                Email = model.Email
            };

            IdentityResult result = await userManager.CreateAsync(user, model.Password);

            if (result.Succeeded)
            {
                await signInManager.SignInAsync(user, isPersistent: false);

                return true;
            }
            return false;
        }
    }
}
