﻿using PetStore.Models;
using PetStore.Models.ViewModels.Error;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PetStore.Services.ActionServices.ErrorActionServices.Interfaces
{
    public interface IErrorBaseService
    {
        public ErrorViewModel Handle(int statusCode, string message);
    }
}
