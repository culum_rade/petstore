﻿using Microsoft.Extensions.Logging;
using PetStore.Controllers;
using PetStore.Models;
using PetStore.Models.ViewModels.Error;
using PetStore.Services.ActionServices.ErrorActionServices.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PetStore.Services.ActionServices.ErrorActionServices
{
    public class ErrorBaseService : IErrorBaseService
    {
        public ErrorViewModel Handle(int statusCode, string message)
        {
            return new ErrorViewModel
            {
                StatusCode = statusCode,
                Message = message
            };
        }
    }
}
