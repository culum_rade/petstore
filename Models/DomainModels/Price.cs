﻿using PetStore.Models.DomainModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace PetStore.Models.DomainModels
{
    public class Price
    {
        public DateTime Since { get; set; }

        public int ItemId { get; set; }

        public Item Item { get; set; }

        [Column(TypeName = "decimal(18, 2)"), Range(0, (double)decimal.MaxValue)]
        public decimal Amount { get; set; }
    }
}
