﻿using PetStore.Models.DomainModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace PetStore.Models.DomainModels
{
    public class Purchase
    {
        public int Id { get; set; }

        [Required]
        public string PurchaserFirstName { get; set; }

        [Required]
        public string PurchaserLatsName { get; set; }

        [Required]
        public string PurchaserAddress { get; set; }

        [Required]
        [Column(TypeName = "varchar(1024)")]
        public string PurchaserCardNumber { get; set; }

        [Required]
        public DateTime DateTime { get; set; }

        [NotMapped]
        public decimal? Total
        {
            get
            {
                return ItemPurchases?.Sum(ip => ip.Quantity *
                    (ip.Item.Prices.
                        Where(i => i.Since.CompareTo(DateTime) < 1).
                        OrderBy(i => i.Since)
                        .Last().Amount));
            }
        }

        public ICollection<ItemPurchase> ItemPurchases { get; set; }
    }
}
