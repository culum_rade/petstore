﻿using PetStore.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace PetStore.Models.DomainModels
{
    public class ItemPurchase
    {
        public int ItemId { get; set; }
        public int PurchaseId { get; set; }

        [Required, Range(1, int.MaxValue)]
        public int Quantity { get; set; }
        public Item Item { get; set; }
        public Purchase Purchase { get; set; }

        [NotMapped]
        public Price Price
        {
            get
            {
                return Item.Prices.Where(i => i.Since.CompareTo(Purchase.DateTime) < 1).
                        OrderBy(i => i.Since)
                        .Last();
            }
        }
    }
}
