﻿using Microsoft.AspNetCore.Mvc;
using PetStore.Enums;
using PetStore.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace PetStore.Models.DomainModels
{
    public abstract class Item
    {
        [Key]
        public int Id { get; set; }

        [Required, MaxLength(200)]
        public string Name { get; set; }
        public string Description { get; set; }
        public string Image { get; set; }
        public ItemUnits ItemUnit { get; set; }

        [DefaultValue(0), Range(0, int.MaxValue)]
        public int InStock { get; set; } = 0;
        public DateTime? DeletedAt { get; set; }
        public ICollection<Price> Prices { get; set; } = new Collection<Price>();
        public ICollection<ItemPurchase> ItemPurchases { get; set; }

        [NotMapped]
        public Price CurrentPrice
        {
            get
            {
                if(Prices != null &&  Prices.Count != 0)
                {
                    return Prices.Last();
                }
                return null;
            }
        }
    }
}
