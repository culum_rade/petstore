using System;

namespace PetStore.Models.ViewModels.Error
{
    public class ErrorViewModel
    {
        public int StatusCode { get; set; }

        public string Message { get; set; }
    }
}
