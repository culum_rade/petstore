﻿using PetStore.Models.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PetStore.Models.ViewModels.Purchases
{
    public class PurchasesIndexViewModel
    {
        public IEnumerable<Purchase> Purchases { get; set; }
        public int CurrentPage { get; set; } = 1;
        public int MaxPage { get; set; }
    }
}
