﻿using PetStore.Models.DomainModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace PetStore.Models.ViewModels.Purchases
{
    public class PurchasesCreateViewModel
    {
        [Required(ErrorMessage = "First Name is required.")]
        public string PurchaserFirstName { get; set; }

        [Required(ErrorMessage = "Last Name is required.")]
        public string PurchaserLatsName { get; set; }

        [Required(ErrorMessage = "Address is required.")]
        public string PurchaserAddress { get; set; }

        [Required(ErrorMessage = "Card Number is required.")]
        [MinLength(5, ErrorMessage = "Card Number is too short.")]
        [MaxLength(20, ErrorMessage = "Card Number is too long.")]
        [Column(TypeName = "varchar(1024)")]
        public string PurchaserCardNumber { get; set; }

        public IList<PurchaseCartItem> PurchesCartItems { get; set; }
        public IList<Toy> Toys { get; set; }

        public bool Successful { get; set; }
        public string Message { get; set; }

    }
}
