﻿using Microsoft.AspNetCore.Identity;
using PetStore.Models.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PetStore.Models.ViewModels.Toys
{
    public class ToysIndexViewModel
    {
        public IEnumerable<Toy> CurrentPageToys { get; set; }
        public int TotalToysCount { get; set; }
        public string SearchString { get; set; }
        public int ItemsPerPage { get; set; }
        public int CurrentPageNumber { get; set; } = 1;
        public int MaxPageNumber
        {
            get
            {
                return (int)Math.Ceiling(TotalToysCount / (float)ItemsPerPage);
            }
        }
    }
}
