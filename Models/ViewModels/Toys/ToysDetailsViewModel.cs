﻿using PetStore.Models.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PetStore.Models.ViewModels.Toys
{
    public class ToysDetailsViewModel
    {
        public Toy Toy { get; set; }
    }
}
