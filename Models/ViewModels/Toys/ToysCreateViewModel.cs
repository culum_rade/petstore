﻿using Microsoft.AspNetCore.Http;
using PetStore.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PetStore.Models.ViewModels.Toys
{
    public class ToysCreateViewModel
    {
        [Required, MaxLength(200)]
        public string Name { get; set; }
        public string Description { get; set; }

        public decimal Price { get; set; }
        public IFormFile ImageFile { get; set; }
        public ItemUnits ItemUnit { get; set; }

        [DefaultValue(0), Range(0, int.MaxValue)]
        public int InStock { get; set; } = 0;

    }
}
