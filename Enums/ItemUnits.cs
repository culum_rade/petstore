﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PetStore.Enums
{
    public enum ItemUnits
    {
        Piece,
        Set,
        Kilogram,
        Acre,
        Yard
    }
}
