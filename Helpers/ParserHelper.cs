﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PetStore.Helpers
{
    public class ParserHelper
    {
        private static ParserHelper parserHelper = null;

        public static ParserHelper Instance
        {
            get
            {
                if (parserHelper == null)
                {
                    parserHelper = new ParserHelper();
                }

                return parserHelper;
            }
        }

        public List<int> TextSeparatedByDelimiterToList(string text, char delimiter)
        {
            List<int> outputList = text != null ? text.Split(delimiter).Select(int.Parse).ToList() : new List<int>();

            return outputList;
        }
    }
}
