﻿using Microsoft.Extensions.DependencyInjection;
using PetStore.Helpers.Interfaces;
using PetStore.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PetStore.Helpers
{
    public static class HelpersInjector
    {
        public static void AddDataHelpers(this IServiceCollection services)
        {
            services.AddSingleton<IFileHelper, FileHelper>();
        }
    }
}
