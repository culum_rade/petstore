﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace PetStore.Helpers
{
    public class CryptoHelper
    {
        private static CryptoHelper cryptoHelper = null;

        public static CryptoHelper Instance
        {
            get
            {
                if (cryptoHelper == null)
                {
                    cryptoHelper = new CryptoHelper();
                }

                return cryptoHelper;
            }
        }

        public string ComputeMD5Hash(string data)
        {
            MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
            string hash = Convert.ToBase64String(md5.ComputeHash(Encoding.ASCII.GetBytes(data)));

            return hash;
        }

    }
}
