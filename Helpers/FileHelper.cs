﻿using Microsoft.AspNetCore.Http;
using PetStore.Helpers.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace PetStore.Web.Helpers
{
    public class FileHelper : IFileHelper
    {
        public async Task UploadFile(IFormFile formFile, string filePath)
        {
            using FileStream fileStream = new FileStream(filePath, FileMode.Create);

            await formFile.CopyToAsync(fileStream);
        }
    }
}
