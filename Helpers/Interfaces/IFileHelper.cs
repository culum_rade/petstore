﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PetStore.Helpers.Interfaces
{
    public interface IFileHelper
    {
        public Task UploadFile(IFormFile formFile, string filePath);
    }
}
