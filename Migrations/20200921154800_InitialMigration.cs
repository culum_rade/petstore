﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PetStore.Migrations
{
    public partial class InitialMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AspNetRoles",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Name = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(maxLength: 256, nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUsers",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    UserName = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedUserName = table.Column<string>(maxLength: 256, nullable: true),
                    Email = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedEmail = table.Column<string>(maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(nullable: false),
                    PasswordHash = table.Column<string>(nullable: true),
                    SecurityStamp = table.Column<string>(nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(nullable: false),
                    TwoFactorEnabled = table.Column<bool>(nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(nullable: true),
                    LockoutEnabled = table.Column<bool>(nullable: false),
                    AccessFailedCount = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUsers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Items",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 200, nullable: false),
                    Description = table.Column<string>(nullable: true),
                    Image = table.Column<string>(nullable: true),
                    ItemUnit = table.Column<int>(nullable: false),
                    InStock = table.Column<int>(nullable: false),
                    DeletedAt = table.Column<DateTime>(nullable: true),
                    Discriminator = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Items", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Purchases",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PurchaserFirstName = table.Column<string>(nullable: false),
                    PurchaserLatsName = table.Column<string>(nullable: false),
                    PurchaserAddress = table.Column<string>(nullable: false),
                    PurchaserCardNumber = table.Column<string>(type: "varchar(1024)", nullable: false),
                    DateTime = table.Column<DateTime>(nullable: false, defaultValueSql: "getdate()")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Purchases", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    RoleId = table.Column<string>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoleClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetRoleClaims_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<string>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUserClaims_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserLogins",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(nullable: false),
                    ProviderKey = table.Column<string>(nullable: false),
                    ProviderDisplayName = table.Column<string>(nullable: true),
                    UserId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserLogins", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_AspNetUserLogins_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserRoles",
                columns: table => new
                {
                    UserId = table.Column<string>(nullable: false),
                    RoleId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserRoles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserTokens",
                columns: table => new
                {
                    UserId = table.Column<string>(nullable: false),
                    LoginProvider = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_AspNetUserTokens_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Prices",
                columns: table => new
                {
                    Since = table.Column<DateTime>(nullable: false, defaultValueSql: "getdate()"),
                    ItemId = table.Column<int>(nullable: false),
                    Amount = table.Column<decimal>(type: "decimal(18, 2)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Prices", x => new { x.Since, x.ItemId });
                    table.ForeignKey(
                        name: "FK_Prices_Items_ItemId",
                        column: x => x.ItemId,
                        principalTable: "Items",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ItemPurchases",
                columns: table => new
                {
                    ItemId = table.Column<int>(nullable: false),
                    PurchaseId = table.Column<int>(nullable: false),
                    Quantity = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ItemPurchases", x => new { x.ItemId, x.PurchaseId });
                    table.ForeignKey(
                        name: "FK_ItemPurchases_Items_ItemId",
                        column: x => x.ItemId,
                        principalTable: "Items",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ItemPurchases_Purchases_PurchaseId",
                        column: x => x.PurchaseId,
                        principalTable: "Purchases",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Items",
                columns: new[] { "Id", "Description", "Discriminator", "Image", "InStock", "ItemUnit", "Name" },
                values: new object[,]
                {
                    { 22, "Description 22", "Toy", "Capture22.PNG", 564, 2, "Pet toy 22" },
                    { 1, "Description 1", "Toy", "Capture1.PNG", 956, 4, "Pet toy 1" },
                    { 2, "Description 2", "Toy", "Capture2.PNG", 582, 2, "Pet toy 2" },
                    { 3, "Description 3", "Toy", "Capture3.PNG", 297, 0, "Pet toy 3" },
                    { 4, "Description 4", "Toy", "Capture4.PNG", 829, 1, "Pet toy 4" },
                    { 5, "Description 5", "Toy", "Capture5.PNG", 777, 0, "Pet toy 5" },
                    { 6, "Description 6", "Toy", "Capture6.PNG", 981, 0, "Pet toy 6" },
                    { 7, "Description 7", "Toy", "Capture7.PNG", 437, 2, "Pet toy 7" },
                    { 8, "Description 8", "Toy", "Capture8.PNG", 795, 2, "Pet toy 8" },
                    { 9, "Description 9", "Toy", "Capture9.PNG", 6, 1, "Pet toy 9" },
                    { 10, "Description 10", "Toy", "Capture10.PNG", 135, 0, "Pet toy 10" },
                    { 21, "Description 21", "Toy", "Capture21.PNG", 417, 3, "Pet toy 21" },
                    { 12, "Description 12", "Toy", "Capture12.PNG", 341, 0, "Pet toy 12" },
                    { 11, "Description 11", "Toy", "Capture11.PNG", 723, 2, "Pet toy 11" },
                    { 19, "Description 19", "Toy", "Capture19.PNG", 17, 4, "Pet toy 19" },
                    { 18, "Description 18", "Toy", "Capture18.PNG", 300, 4, "Pet toy 18" },
                    { 17, "Description 17", "Toy", "Capture17.PNG", 110, 2, "Pet toy 17" },
                    { 20, "Description 20", "Toy", "Capture20.PNG", 547, 0, "Pet toy 20" },
                    { 15, "Description 15", "Toy", "Capture15.PNG", 736, 3, "Pet toy 15" },
                    { 14, "Description 14", "Toy", "Capture14.PNG", 718, 2, "Pet toy 14" },
                    { 13, "Description 13", "Toy", "Capture13.PNG", 91, 4, "Pet toy 13" },
                    { 16, "Description 16", "Toy", "Capture16.PNG", 745, 4, "Pet toy 16" }
                });

            migrationBuilder.InsertData(
                table: "Purchases",
                columns: new[] { "Id", "DateTime", "PurchaserAddress", "PurchaserCardNumber", "PurchaserFirstName", "PurchaserLatsName" },
                values: new object[,]
                {
                    { 73, new DateTime(2020, 8, 7, 17, 47, 59, 653, DateTimeKind.Local).AddTicks(551), "Address73", "Ahl5jcDHylh6/hPk7Uox2A==", "Name73", "LastName73" },
                    { 78, new DateTime(2020, 7, 22, 17, 47, 59, 653, DateTimeKind.Local).AddTicks(1367), "Address78", "cVORpxipKJISSukEkdenSA==", "Name78", "LastName78" },
                    { 77, new DateTime(2020, 9, 4, 17, 47, 59, 653, DateTimeKind.Local).AddTicks(1229), "Address77", "sdWKp2YVilddr+5nvMeEFw==", "Name77", "LastName77" },
                    { 76, new DateTime(2020, 9, 12, 17, 47, 59, 653, DateTimeKind.Local).AddTicks(1039), "Address76", "dDeiEkqLw5gEMlmUngJS8w==", "Name76", "LastName76" },
                    { 75, new DateTime(2020, 9, 9, 17, 47, 59, 653, DateTimeKind.Local).AddTicks(823), "Address75", "wPBlf4LugDgdYwEjc4tH9Q==", "Name75", "LastName75" },
                    { 74, new DateTime(2020, 8, 20, 17, 47, 59, 653, DateTimeKind.Local).AddTicks(640), "Address74", "NcfWFTP9uDEZjsv1bJGfQA==", "Name74", "LastName74" },
                    { 72, new DateTime(2020, 6, 13, 17, 47, 59, 653, DateTimeKind.Local).AddTicks(388), "Address72", "1biUPLxWrx29F9EVEvicog==", "Name72", "LastName72" },
                    { 67, new DateTime(2020, 9, 16, 17, 47, 59, 652, DateTimeKind.Local).AddTicks(9780), "Address67", "9jHnoZQ7sJytNl1JAcVQ1w==", "Name67", "LastName67" },
                    { 70, new DateTime(2020, 7, 28, 17, 47, 59, 653, DateTimeKind.Local).AddTicks(81), "Address70", "qWzpUokNDkx5/C4RNc39RQ==", "Name70", "LastName70" },
                    { 69, new DateTime(2020, 9, 7, 17, 47, 59, 653, DateTimeKind.Local).AddTicks(13), "Address69", "02fPVIesleT3DO9eDSqU3g==", "Name69", "LastName69" },
                    { 68, new DateTime(2020, 9, 7, 17, 47, 59, 652, DateTimeKind.Local).AddTicks(9917), "Address68", "RqPFD45Xn/DRatUWwJg8/g==", "Name68", "LastName68" },
                    { 79, new DateTime(2020, 9, 8, 17, 47, 59, 653, DateTimeKind.Local).AddTicks(1544), "Address79", "mKCE0q+NBBAi7yeBrXh3og==", "Name79", "LastName79" },
                    { 66, new DateTime(2020, 8, 28, 17, 47, 59, 652, DateTimeKind.Local).AddTicks(9671), "Address66", "JBJbEhRTCJbe27F5JGA87A==", "Name66", "LastName66" },
                    { 65, new DateTime(2020, 9, 6, 17, 47, 59, 652, DateTimeKind.Local).AddTicks(9601), "Address65", "ZNz0cenox8nEQImUKlwgIA==", "Name65", "LastName65" },
                    { 64, new DateTime(2020, 7, 13, 17, 47, 59, 652, DateTimeKind.Local).AddTicks(9529), "Address64", "XECNjJWoB0QORURpZWs3qg==", "Name64", "LastName64" },
                    { 63, new DateTime(2020, 7, 19, 17, 47, 59, 652, DateTimeKind.Local).AddTicks(9405), "Address63", "3TijEh45vKfRYVac2P462w==", "Name63", "LastName63" },
                    { 71, new DateTime(2020, 8, 8, 17, 47, 59, 653, DateTimeKind.Local).AddTicks(258), "Address71", "6cfMO+qGATFu3aVvKtC0Tg==", "Name71", "LastName71" },
                    { 80, new DateTime(2020, 6, 13, 17, 47, 59, 653, DateTimeKind.Local).AddTicks(1598), "Address80", "Lt5204Q8J8rvL4AXl9hjXw==", "Name80", "LastName80" },
                    { 85, new DateTime(2020, 6, 14, 17, 47, 59, 653, DateTimeKind.Local).AddTicks(2240), "Address85", "qy4qCsZx/XaRlCy1bR/zCg==", "Name85", "LastName85" },
                    { 82, new DateTime(2020, 9, 12, 17, 47, 59, 653, DateTimeKind.Local).AddTicks(1914), "Address82", "k/dciIXo/5fmGaT76JRH+A==", "Name82", "LastName82" },
                    { 99, new DateTime(2020, 7, 11, 17, 47, 59, 653, DateTimeKind.Local).AddTicks(4116), "Address99", "toik/MOjVq5/u3yMM0vLAA==", "Name99", "LastName99" },
                    { 98, new DateTime(2020, 7, 6, 17, 47, 59, 653, DateTimeKind.Local).AddTicks(3941), "Address98", "PgqQqQEohrdwSYueLIPrng==", "Name98", "LastName98" },
                    { 97, new DateTime(2020, 7, 20, 17, 47, 59, 653, DateTimeKind.Local).AddTicks(3782), "Address97", "TNyn6UEVmyToNVjMlUpYxw==", "Name97", "LastName97" },
                    { 96, new DateTime(2020, 9, 1, 17, 47, 59, 653, DateTimeKind.Local).AddTicks(3685), "Address96", "hZk1iXQK4k7toV6xbIqS8w==", "Name96", "LastName96" },
                    { 95, new DateTime(2020, 7, 29, 17, 47, 59, 653, DateTimeKind.Local).AddTicks(3442), "Address95", "EJ0j5Qb7G3M/yw3dYhrvkA==", "Name95", "LastName95" },
                    { 94, new DateTime(2020, 8, 26, 17, 47, 59, 653, DateTimeKind.Local).AddTicks(3373), "Address94", "nLd6UTtcncsvbYRVT8ASaA==", "Name94", "LastName94" },
                    { 93, new DateTime(2020, 7, 16, 17, 47, 59, 653, DateTimeKind.Local).AddTicks(3203), "Address93", "fNvrYUzdjuvMzK5wKLat/Q==", "Name93", "LastName93" },
                    { 81, new DateTime(2020, 7, 9, 17, 47, 59, 653, DateTimeKind.Local).AddTicks(1722), "Address81", "hpxIa/X6hCeWgoVAK/MLtg==", "Name81", "LastName81" },
                    { 92, new DateTime(2020, 6, 23, 17, 47, 59, 653, DateTimeKind.Local).AddTicks(3084), "Address92", "pAmPHVqEZp7wohUbt3Xhxw==", "Name92", "LastName92" },
                    { 90, new DateTime(2020, 8, 27, 17, 47, 59, 653, DateTimeKind.Local).AddTicks(2869), "Address90", "LiPAZmWZoYN0xlSQ2+7Jxw==", "Name90", "LastName90" },
                    { 89, new DateTime(2020, 6, 18, 17, 47, 59, 653, DateTimeKind.Local).AddTicks(2785), "Address89", "YBv2SrTOE33N+/xaC604iQ==", "Name89", "LastName89" },
                    { 88, new DateTime(2020, 9, 16, 17, 47, 59, 653, DateTimeKind.Local).AddTicks(2626), "Address88", "FkS7rgOKjbmAlFodMiMEXw==", "Name88", "LastName88" },
                    { 87, new DateTime(2020, 7, 25, 17, 47, 59, 653, DateTimeKind.Local).AddTicks(2477), "Address87", "Yx91AHYgKkBQ79mZkRYYKA==", "Name87", "LastName87" },
                    { 86, new DateTime(2020, 8, 30, 17, 47, 59, 653, DateTimeKind.Local).AddTicks(2353), "Address86", "NsoOp5cSRn7NqVlgTdrHLw==", "Name86", "LastName86" },
                    { 84, new DateTime(2020, 8, 26, 17, 47, 59, 653, DateTimeKind.Local).AddTicks(2122), "Address84", "2JdPQQepXgfs1tVOo0r4kA==", "Name84", "LastName84" },
                    { 83, new DateTime(2020, 6, 19, 17, 47, 59, 653, DateTimeKind.Local).AddTicks(1971), "Address83", "R/AyDPzH5J04TS9kHKoN+w==", "Name83", "LastName83" },
                    { 62, new DateTime(2020, 8, 14, 17, 47, 59, 652, DateTimeKind.Local).AddTicks(9227), "Address62", "4qtSgit3keeqUYr6fFNo6Q==", "Name62", "LastName62" },
                    { 91, new DateTime(2020, 9, 8, 17, 47, 59, 653, DateTimeKind.Local).AddTicks(2936), "Address91", "4sbZFzfFHZdZIt6qyYGMLw==", "Name91", "LastName91" },
                    { 61, new DateTime(2020, 7, 26, 17, 47, 59, 652, DateTimeKind.Local).AddTicks(9144), "Address61", "1Ow+EoqboYAHUVtacwPFzw==", "Name61", "LastName61" },
                    { 59, new DateTime(2020, 9, 1, 17, 47, 59, 652, DateTimeKind.Local).AddTicks(8884), "Address59", "GdA3UY2/CM7bTM93k2DhQg==", "Name59", "LastName59" },
                    { 27, new DateTime(2020, 9, 2, 17, 47, 59, 652, DateTimeKind.Local).AddTicks(4457), "Address27", "ew7WAev0Sb2Op+czIhxVpw==", "Name27", "LastName27" },
                    { 26, new DateTime(2020, 7, 9, 17, 47, 59, 652, DateTimeKind.Local).AddTicks(4362), "Address26", "Wwhe9QxGX+P5PKRSCjpyHg==", "Name26", "LastName26" },
                    { 25, new DateTime(2020, 9, 3, 17, 47, 59, 652, DateTimeKind.Local).AddTicks(4295), "Address25", "50/EDzgm5eruLBSRXz1kzQ==", "Name25", "LastName25" },
                    { 24, new DateTime(2020, 9, 19, 17, 47, 59, 652, DateTimeKind.Local).AddTicks(4191), "Address24", "U4SJKxe63NUOyA/xTsiehg==", "Name24", "LastName24" },
                    { 23, new DateTime(2020, 7, 22, 17, 47, 59, 652, DateTimeKind.Local).AddTicks(3978), "Address23", "Eu2Ryw3J+20eqL9IIrKo6w==", "Name23", "LastName23" },
                    { 22, new DateTime(2020, 8, 6, 17, 47, 59, 652, DateTimeKind.Local).AddTicks(3669), "Address22", "xMnNJN55Hm3EyEwRoktCjw==", "Name22", "LastName22" },
                    { 21, new DateTime(2020, 9, 17, 17, 47, 59, 652, DateTimeKind.Local).AddTicks(3502), "Address21", "0G2udmcwVLzFaGQjnqhcHw==", "Name21", "LastName21" },
                    { 20, new DateTime(2020, 9, 12, 17, 47, 59, 652, DateTimeKind.Local).AddTicks(3328), "Address20", "31w+X/LSErH+WBTXTKXVAw==", "Name20", "LastName20" },
                    { 19, new DateTime(2020, 7, 14, 17, 47, 59, 652, DateTimeKind.Local).AddTicks(3164), "Address19", "8BLi104gH0roskql1uzaaQ==", "Name19", "LastName19" },
                    { 18, new DateTime(2020, 7, 28, 17, 47, 59, 652, DateTimeKind.Local).AddTicks(3010), "Address18", "kdAvAwpiJSFLsxVfgrKg9A==", "Name18", "LastName18" },
                    { 17, new DateTime(2020, 8, 5, 17, 47, 59, 652, DateTimeKind.Local).AddTicks(2874), "Address17", "o9f5ZytIduCPnvN5IgXn7g==", "Name17", "LastName17" },
                    { 16, new DateTime(2020, 8, 8, 17, 47, 59, 652, DateTimeKind.Local).AddTicks(2750), "Address16", "pzesSWVwzwFcV8TaKDsUWQ==", "Name16", "LastName16" },
                    { 28, new DateTime(2020, 8, 21, 17, 47, 59, 652, DateTimeKind.Local).AddTicks(4634), "Address28", "E8yGu13vO4XiPxrZK4TFRA==", "Name28", "LastName28" },
                    { 15, new DateTime(2020, 7, 28, 17, 47, 59, 652, DateTimeKind.Local).AddTicks(2622), "Address15", "RKmkjsDUyDsnDTwlVlzrxA==", "Name15", "LastName15" },
                    { 13, new DateTime(2020, 8, 23, 17, 47, 59, 652, DateTimeKind.Local).AddTicks(2323), "Address13", "zWIpw85SgxztfrjAAyGwXQ==", "Name13", "LastName13" },
                    { 12, new DateTime(2020, 9, 18, 17, 47, 59, 652, DateTimeKind.Local).AddTicks(2128), "Address12", "zJuyLVUHe9/aGtjuLkGZtQ==", "Name12", "LastName12" },
                    { 11, new DateTime(2020, 9, 4, 17, 47, 59, 652, DateTimeKind.Local).AddTicks(2074), "Address11", "hbdWHD0l+RpH11vORZNu+w==", "Name11", "LastName11" },
                    { 10, new DateTime(2020, 7, 19, 17, 47, 59, 652, DateTimeKind.Local).AddTicks(1973), "Address10", "ZmiYZb+fYa6P0L6vOXjQjQ==", "Name10", "LastName10" },
                    { 9, new DateTime(2020, 8, 7, 17, 47, 59, 652, DateTimeKind.Local).AddTicks(1762), "Address9", "NgNKeZ2eoECnXIoo8VF6Og==", "Name9", "LastName9" },
                    { 8, new DateTime(2020, 7, 1, 17, 47, 59, 652, DateTimeKind.Local).AddTicks(1653), "Address8", "CYu53cARnW6Fv+CYUaRf/A==", "Name8", "LastName8" },
                    { 7, new DateTime(2020, 6, 24, 17, 47, 59, 652, DateTimeKind.Local).AddTicks(1571), "Address7", "0WPtyXm/JHsbdfFaiMRKFQ==", "Name7", "LastName7" },
                    { 6, new DateTime(2020, 8, 8, 17, 47, 59, 652, DateTimeKind.Local).AddTicks(1498), "Address6", "xm6REkBHsU6iJtZiFGBEFQ==", "Name6", "LastName6" },
                    { 5, new DateTime(2020, 8, 20, 17, 47, 59, 652, DateTimeKind.Local).AddTicks(1372), "Address5", "ZugZTJnvwYwzMTEeea4q7A==", "Name5", "LastName5" },
                    { 4, new DateTime(2020, 7, 12, 17, 47, 59, 652, DateTimeKind.Local).AddTicks(1201), "Address4", "RlkHQWU8p5nWL9nX5ZQQaA==", "Name4", "LastName4" },
                    { 3, new DateTime(2020, 6, 27, 17, 47, 59, 652, DateTimeKind.Local).AddTicks(887), "Address3", "g9P1uhV2efIrE3Nzr9KXFQ==", "Name3", "LastName3" },
                    { 2, new DateTime(2020, 7, 23, 17, 47, 59, 652, DateTimeKind.Local).AddTicks(732), "Address2", "gKRK3cAYYCLvWSd0eLqwlQ==", "Name2", "LastName2" },
                    { 14, new DateTime(2020, 8, 13, 17, 47, 59, 652, DateTimeKind.Local).AddTicks(2514), "Address14", "rKVYbYIoc6mB+fcz2HIezw==", "Name14", "LastName14" },
                    { 29, new DateTime(2020, 6, 20, 17, 47, 59, 652, DateTimeKind.Local).AddTicks(4773), "Address29", "29azKU4bp+gDUkjBF78kNQ==", "Name29", "LastName29" },
                    { 30, new DateTime(2020, 7, 18, 17, 47, 59, 652, DateTimeKind.Local).AddTicks(4828), "Address30", "v+kRMp+2cqivMJrAs2wa1A==", "Name30", "LastName30" },
                    { 31, new DateTime(2020, 7, 4, 17, 47, 59, 652, DateTimeKind.Local).AddTicks(4895), "Address31", "kivdd1y6QQ6+2R5qUW7aCw==", "Name31", "LastName31" },
                    { 58, new DateTime(2020, 6, 14, 17, 47, 59, 652, DateTimeKind.Local).AddTicks(8710), "Address58", "ug9tBhXVCzuuZ+TJxjdNNg==", "Name58", "LastName58" },
                    { 57, new DateTime(2020, 9, 20, 17, 47, 59, 652, DateTimeKind.Local).AddTicks(8482), "Address57", "TXqLbLjVVNliOeMGX716hg==", "Name57", "LastName57" },
                    { 56, new DateTime(2020, 8, 27, 17, 47, 59, 652, DateTimeKind.Local).AddTicks(8312), "Address56", "+hD23UX3UoYzgTcAncr3uw==", "Name56", "LastName56" },
                    { 55, new DateTime(2020, 8, 3, 17, 47, 59, 652, DateTimeKind.Local).AddTicks(8107), "Address55", "MQCt42KP1VaHvsAe8xMr/w==", "Name55", "LastName55" },
                    { 54, new DateTime(2020, 7, 5, 17, 47, 59, 652, DateTimeKind.Local).AddTicks(7995), "Address54", "9vYiZZ7Nh6LrDGlDo5wsRA==", "Name54", "LastName54" },
                    { 53, new DateTime(2020, 7, 20, 17, 47, 59, 652, DateTimeKind.Local).AddTicks(7834), "Address53", "LdmAKgPQo/+2ogbWfVA4NA==", "Name53", "LastName53" },
                    { 52, new DateTime(2020, 8, 20, 17, 47, 59, 652, DateTimeKind.Local).AddTicks(7697), "Address52", "D61lVnjkt88RhlRhGpOKCA==", "Name52", "LastName52" },
                    { 51, new DateTime(2020, 6, 25, 17, 47, 59, 652, DateTimeKind.Local).AddTicks(7559), "Address51", "E3pZHe9BKqzHR/MnP4zP3g==", "Name51", "LastName51" },
                    { 50, new DateTime(2020, 6, 13, 17, 47, 59, 652, DateTimeKind.Local).AddTicks(7453), "Address50", "9pRzFqqBvx9yeQwB7CxkTw==", "Name50", "LastName50" },
                    { 49, new DateTime(2020, 9, 20, 17, 47, 59, 652, DateTimeKind.Local).AddTicks(7329), "Address49", "wLg8ak9tQIBNSrCsFQls4w==", "Name49", "LastName49" },
                    { 48, new DateTime(2020, 8, 25, 17, 47, 59, 652, DateTimeKind.Local).AddTicks(7277), "Address48", "ReT4h27SvyoQ0l0riXMwMQ==", "Name48", "LastName48" },
                    { 47, new DateTime(2020, 7, 13, 17, 47, 59, 652, DateTimeKind.Local).AddTicks(7180), "Address47", "BZUX8dX74zHRdL3/RjynCA==", "Name47", "LastName47" },
                    { 46, new DateTime(2020, 7, 28, 17, 47, 59, 652, DateTimeKind.Local).AddTicks(7041), "Address46", "drOUDZk1y1Lq9RuSV8Eqaw==", "Name46", "LastName46" },
                    { 45, new DateTime(2020, 7, 22, 17, 47, 59, 652, DateTimeKind.Local).AddTicks(6898), "Address45", "QuMVTRrDQtnZ9xw5h0xiqg==", "Name45", "LastName45" },
                    { 44, new DateTime(2020, 7, 31, 17, 47, 59, 652, DateTimeKind.Local).AddTicks(6753), "Address44", "8HIaVT0g16FjmCpACCCCJA==", "Name44", "LastName44" },
                    { 43, new DateTime(2020, 6, 21, 17, 47, 59, 652, DateTimeKind.Local).AddTicks(6600), "Address43", "F1AiQ8luywFYXfU+jORpKQ==", "Name43", "LastName43" },
                    { 42, new DateTime(2020, 7, 17, 17, 47, 59, 652, DateTimeKind.Local).AddTicks(6381), "Address42", "krBAcf2HmNUM5AvhhSpG3w==", "Name42", "LastName42" },
                    { 41, new DateTime(2020, 9, 11, 17, 47, 59, 652, DateTimeKind.Local).AddTicks(6258), "Address41", "lI2+Glawja79XOx1ED9pbw==", "Name41", "LastName41" },
                    { 40, new DateTime(2020, 9, 5, 17, 47, 59, 652, DateTimeKind.Local).AddTicks(6176), "Address40", "ZQcNYj2xbqFmhaxT2FHQyg==", "Name40", "LastName40" },
                    { 39, new DateTime(2020, 7, 25, 17, 47, 59, 652, DateTimeKind.Local).AddTicks(6011), "Address39", "dukCN7YwzVF/ic4Xyhnt2w==", "Name39", "LastName39" },
                    { 38, new DateTime(2020, 7, 9, 17, 47, 59, 652, DateTimeKind.Local).AddTicks(5955), "Address38", "yOur4qwrKLd+lQU8dPNgcw==", "Name38", "LastName38" },
                    { 37, new DateTime(2020, 7, 29, 17, 47, 59, 652, DateTimeKind.Local).AddTicks(5814), "Address37", "Y71J1Y/AK12IT7kJ3EXRUw==", "Name37", "LastName37" },
                    { 36, new DateTime(2020, 8, 30, 17, 47, 59, 652, DateTimeKind.Local).AddTicks(5667), "Address36", "gP1oSSHLBcrSacV4ujJvBQ==", "Name36", "LastName36" },
                    { 35, new DateTime(2020, 7, 7, 17, 47, 59, 652, DateTimeKind.Local).AddTicks(5489), "Address35", "5pgusxSlNAuYocz/Jg2I3w==", "Name35", "LastName35" },
                    { 34, new DateTime(2020, 9, 1, 17, 47, 59, 652, DateTimeKind.Local).AddTicks(5391), "Address34", "sEeEiahGsmz7XwiAEnUSdA==", "Name34", "LastName34" },
                    { 33, new DateTime(2020, 9, 1, 17, 47, 59, 652, DateTimeKind.Local).AddTicks(5211), "Address33", "Lz2QAzqdiqHqF14EeO56nQ==", "Name33", "LastName33" },
                    { 32, new DateTime(2020, 8, 28, 17, 47, 59, 652, DateTimeKind.Local).AddTicks(5085), "Address32", "rKE0f5F8Wb0oFHXd5+IdyQ==", "Name32", "LastName32" },
                    { 60, new DateTime(2020, 9, 20, 17, 47, 59, 652, DateTimeKind.Local).AddTicks(8994), "Address60", "S6dqvb/dFsitZfgR/l75TA==", "Name60", "LastName60" },
                    { 1, new DateTime(2020, 6, 13, 17, 47, 59, 651, DateTimeKind.Local).AddTicks(7723), "Address1", "0dNLhj5WB+jxTcXgi6z2AA==", "Name1", "LastName1" }
                });

            migrationBuilder.InsertData(
                table: "ItemPurchases",
                columns: new[] { "ItemId", "PurchaseId", "Quantity" },
                values: new object[,]
                {
                    { 1, 1, 8 },
                    { 10, 74, 7 },
                    { 10, 72, 5 },
                    { 10, 70, 5 },
                    { 10, 68, 1 },
                    { 10, 66, 5 },
                    { 10, 64, 2 },
                    { 10, 63, 2 },
                    { 10, 60, 5 },
                    { 10, 59, 2 },
                    { 10, 58, 2 },
                    { 10, 57, 1 },
                    { 10, 56, 1 },
                    { 10, 55, 5 },
                    { 10, 53, 7 },
                    { 10, 75, 1 },
                    { 10, 52, 7 },
                    { 10, 46, 4 },
                    { 10, 45, 0 },
                    { 10, 43, 7 },
                    { 10, 42, 8 },
                    { 10, 41, 2 },
                    { 10, 37, 10 },
                    { 10, 36, 9 },
                    { 10, 35, 3 },
                    { 10, 33, 9 },
                    { 10, 28, 8 },
                    { 10, 22, 9 },
                    { 10, 21, 5 },
                    { 10, 20, 9 },
                    { 10, 19, 3 },
                    { 10, 47, 8 },
                    { 10, 77, 2 },
                    { 10, 81, 2 },
                    { 10, 83, 3 },
                    { 11, 52, 0 },
                    { 11, 47, 3 },
                    { 11, 43, 10 },
                    { 11, 42, 0 },
                    { 11, 41, 4 },
                    { 11, 37, 2 },
                    { 11, 36, 7 },
                    { 11, 35, 4 },
                    { 11, 33, 10 },
                    { 11, 28, 1 },
                    { 11, 22, 2 },
                    { 11, 21, 0 },
                    { 11, 19, 5 },
                    { 11, 18, 10 },
                    { 11, 16, 6 },
                    { 11, 15, 10 },
                    { 11, 13, 2 },
                    { 10, 85, 2 },
                    { 10, 86, 10 },
                    { 10, 87, 6 },
                    { 10, 88, 3 },
                    { 10, 90, 7 },
                    { 10, 93, 8 },
                    { 10, 18, 2 },
                    { 10, 94, 4 },
                    { 10, 97, 9 },
                    { 10, 98, 7 },
                    { 10, 99, 3 },
                    { 11, 3, 7 },
                    { 11, 4, 10 },
                    { 11, 9, 3 },
                    { 10, 95, 3 },
                    { 11, 53, 7 },
                    { 10, 17, 3 },
                    { 10, 15, 7 },
                    { 9, 47, 0 },
                    { 9, 46, 9 },
                    { 9, 45, 9 },
                    { 9, 42, 3 },
                    { 9, 41, 2 },
                    { 9, 40, 6 },
                    { 9, 37, 8 },
                    { 9, 36, 7 },
                    { 9, 35, 8 },
                    { 9, 33, 2 },
                    { 9, 31, 7 },
                    { 9, 28, 1 },
                    { 9, 22, 8 },
                    { 9, 21, 4 },
                    { 9, 50, 0 },
                    { 9, 20, 8 },
                    { 9, 17, 8 },
                    { 9, 16, 0 },
                    { 9, 15, 3 },
                    { 9, 13, 0 },
                    { 9, 9, 9 },
                    { 9, 4, 1 },
                    { 9, 3, 7 },
                    { 8, 99, 0 },
                    { 8, 97, 1 },
                    { 8, 95, 1 },
                    { 8, 93, 7 },
                    { 8, 91, 5 },
                    { 8, 89, 7 },
                    { 8, 88, 9 },
                    { 9, 18, 0 },
                    { 9, 52, 0 },
                    { 9, 53, 6 },
                    { 9, 55, 4 },
                    { 10, 13, 0 },
                    { 10, 9, 5 },
                    { 10, 4, 10 },
                    { 10, 3, 10 },
                    { 9, 99, 7 },
                    { 9, 98, 5 },
                    { 9, 97, 9 },
                    { 9, 96, 9 },
                    { 9, 95, 10 },
                    { 9, 93, 2 },
                    { 9, 88, 0 },
                    { 9, 87, 10 },
                    { 9, 85, 2 },
                    { 9, 83, 2 },
                    { 9, 81, 0 },
                    { 9, 79, 5 },
                    { 9, 77, 10 },
                    { 9, 56, 5 },
                    { 9, 57, 10 },
                    { 9, 58, 9 },
                    { 9, 60, 3 },
                    { 9, 62, 2 },
                    { 9, 63, 6 },
                    { 10, 16, 0 },
                    { 9, 64, 0 },
                    { 9, 68, 7 },
                    { 9, 69, 6 },
                    { 9, 70, 2 },
                    { 9, 72, 3 },
                    { 9, 74, 6 },
                    { 9, 75, 9 },
                    { 9, 65, 10 },
                    { 11, 55, 9 },
                    { 11, 56, 5 },
                    { 11, 57, 6 },
                    { 14, 59, 7 },
                    { 14, 56, 0 },
                    { 14, 43, 8 },
                    { 14, 42, 7 },
                    { 14, 37, 10 },
                    { 14, 35, 4 },
                    { 14, 33, 8 },
                    { 14, 28, 10 },
                    { 14, 22, 4 },
                    { 14, 21, 10 },
                    { 14, 19, 10 },
                    { 14, 18, 2 },
                    { 14, 13, 3 },
                    { 14, 4, 5 },
                    { 14, 66, 10 },
                    { 13, 99, 5 },
                    { 13, 97, 8 },
                    { 13, 86, 4 },
                    { 13, 83, 4 },
                    { 13, 77, 2 },
                    { 13, 74, 7 },
                    { 13, 72, 7 },
                    { 13, 70, 6 },
                    { 13, 66, 8 },
                    { 13, 59, 4 },
                    { 13, 58, 7 },
                    { 13, 56, 4 },
                    { 13, 55, 7 },
                    { 13, 52, 0 },
                    { 13, 43, 10 },
                    { 13, 98, 6 },
                    { 14, 72, 5 },
                    { 14, 74, 7 },
                    { 14, 83, 8 },
                    { 18, 74, 3 },
                    { 18, 19, 4 },
                    { 17, 74, 6 },
                    { 17, 56, 1 },
                    { 17, 43, 9 },
                    { 17, 19, 9 },
                    { 17, 4, 3 },
                    { 16, 97, 1 },
                    { 16, 74, 1 },
                    { 16, 56, 4 },
                    { 16, 43, 0 },
                    { 16, 22, 5 },
                    { 16, 19, 0 },
                    { 16, 4, 8 },
                    { 15, 98, 3 },
                    { 15, 97, 10 },
                    { 15, 86, 10 },
                    { 14, 86, 2 },
                    { 14, 97, 3 },
                    { 14, 98, 6 },
                    { 15, 4, 10 },
                    { 15, 13, 4 },
                    { 15, 19, 8 },
                    { 13, 42, 9 },
                    { 15, 21, 7 },
                    { 15, 28, 3 },
                    { 15, 35, 1 },
                    { 15, 43, 2 },
                    { 15, 56, 0 },
                    { 15, 74, 6 },
                    { 15, 83, 4 },
                    { 15, 22, 4 },
                    { 13, 37, 10 },
                    { 13, 35, 4 },
                    { 13, 33, 9 },
                    { 12, 22, 7 },
                    { 12, 21, 10 },
                    { 12, 19, 1 },
                    { 12, 18, 7 },
                    { 12, 16, 4 },
                    { 12, 15, 7 },
                    { 12, 13, 3 },
                    { 12, 9, 7 },
                    { 12, 4, 6 },
                    { 12, 3, 4 },
                    { 11, 99, 3 },
                    { 11, 98, 6 },
                    { 11, 97, 10 },
                    { 11, 95, 6 },
                    { 11, 94, 0 },
                    { 11, 93, 6 },
                    { 11, 90, 1 },
                    { 11, 58, 5 },
                    { 11, 59, 3 },
                    { 11, 63, 2 },
                    { 11, 66, 8 },
                    { 11, 70, 3 },
                    { 11, 72, 1 },
                    { 12, 28, 10 },
                    { 11, 74, 8 },
                    { 11, 77, 7 },
                    { 11, 81, 2 },
                    { 11, 83, 5 },
                    { 11, 85, 10 },
                    { 11, 86, 0 },
                    { 11, 88, 0 },
                    { 11, 75, 10 },
                    { 8, 87, 2 },
                    { 12, 33, 9 },
                    { 12, 36, 10 },
                    { 13, 28, 9 },
                    { 13, 22, 3 },
                    { 13, 21, 9 },
                    { 13, 19, 5 },
                    { 13, 18, 4 },
                    { 13, 16, 3 },
                    { 13, 13, 4 },
                    { 13, 9, 7 },
                    { 13, 4, 3 },
                    { 12, 99, 3 },
                    { 12, 98, 2 },
                    { 12, 97, 10 },
                    { 12, 93, 5 },
                    { 12, 86, 5 },
                    { 12, 83, 5 },
                    { 12, 81, 6 },
                    { 12, 77, 2 },
                    { 12, 37, 1 },
                    { 12, 42, 9 },
                    { 12, 43, 5 },
                    { 12, 47, 7 },
                    { 12, 52, 2 },
                    { 12, 53, 5 },
                    { 12, 35, 2 },
                    { 12, 55, 1 },
                    { 12, 58, 4 },
                    { 12, 59, 1 },
                    { 12, 66, 2 },
                    { 12, 70, 8 },
                    { 12, 72, 8 },
                    { 12, 74, 0 },
                    { 12, 56, 5 },
                    { 8, 85, 4 },
                    { 8, 96, 0 },
                    { 8, 82, 6 },
                    { 5, 10, 8 },
                    { 5, 9, 7 },
                    { 5, 8, 1 },
                    { 5, 6, 10 },
                    { 5, 5, 4 },
                    { 5, 3, 4 },
                    { 5, 2, 6 },
                    { 5, 1, 7 },
                    { 4, 99, 4 },
                    { 4, 93, 8 },
                    { 4, 91, 5 },
                    { 4, 80, 2 },
                    { 4, 78, 8 },
                    { 4, 76, 0 },
                    { 5, 12, 6 },
                    { 4, 75, 2 },
                    { 4, 67, 2 },
                    { 4, 62, 8 },
                    { 4, 57, 7 },
                    { 4, 54, 7 },
                    { 4, 51, 2 },
                    { 4, 49, 2 },
                    { 4, 45, 3 },
                    { 4, 44, 6 },
                    { 4, 39, 6 },
                    { 4, 38, 5 },
                    { 4, 34, 10 },
                    { 4, 32, 10 },
                    { 4, 31, 6 },
                    { 4, 30, 9 },
                    { 4, 71, 10 },
                    { 5, 20, 7 },
                    { 5, 23, 7 },
                    { 5, 26, 6 },
                    { 6, 12, 4 },
                    { 6, 11, 4 },
                    { 6, 10, 7 },
                    { 6, 9, 7 },
                    { 6, 3, 5 },
                    { 8, 83, 3 },
                    { 5, 99, 7 },
                    { 5, 93, 2 },
                    { 5, 91, 4 },
                    { 5, 84, 0 },
                    { 5, 81, 2 },
                    { 5, 80, 2 },
                    { 5, 78, 0 },
                    { 5, 76, 10 },
                    { 5, 75, 5 },
                    { 5, 71, 2 },
                    { 5, 62, 9 },
                    { 5, 27, 3 },
                    { 5, 31, 3 },
                    { 5, 32, 5 },
                    { 5, 34, 7 },
                    { 5, 39, 10 },
                    { 5, 42, 3 },
                    { 4, 27, 6 },
                    { 5, 44, 4 },
                    { 5, 49, 4 },
                    { 5, 51, 0 },
                    { 5, 54, 1 },
                    { 5, 55, 4 },
                    { 5, 57, 4 },
                    { 5, 58, 5 },
                    { 5, 45, 6 },
                    { 6, 14, 6 },
                    { 4, 26, 8 },
                    { 4, 23, 1 },
                    { 2, 92, 8 },
                    { 2, 78, 8 },
                    { 2, 76, 2 },
                    { 2, 71, 8 },
                    { 2, 67, 7 },
                    { 2, 62, 10 },
                    { 2, 51, 3 },
                    { 2, 49, 4 },
                    { 2, 39, 10 },
                    { 2, 34, 7 },
                    { 2, 31, 6 },
                    { 2, 27, 5 },
                    { 2, 25, 6 },
                    { 2, 24, 7 },
                    { 3, 1, 8 },
                    { 2, 23, 9 },
                    { 2, 8, 6 },
                    { 2, 7, 6 },
                    { 2, 1, 0 },
                    { 1, 92, 10 },
                    { 1, 78, 8 },
                    { 1, 76, 8 },
                    { 1, 67, 10 },
                    { 1, 51, 8 },
                    { 1, 27, 9 },
                    { 1, 24, 7 },
                    { 1, 23, 0 },
                    { 1, 12, 2 },
                    { 1, 8, 6 },
                    { 1, 7, 6 },
                    { 2, 12, 0 },
                    { 3, 3, 9 },
                    { 3, 5, 10 },
                    { 3, 7, 3 },
                    { 4, 20, 9 },
                    { 4, 12, 4 },
                    { 4, 10, 6 },
                    { 4, 8, 10 },
                    { 4, 6, 6 },
                    { 4, 5, 5 },
                    { 4, 3, 9 },
                    { 4, 1, 7 },
                    { 3, 91, 10 },
                    { 3, 80, 9 },
                    { 3, 78, 6 },
                    { 3, 76, 5 },
                    { 3, 75, 6 },
                    { 3, 71, 5 },
                    { 3, 67, 3 },
                    { 3, 62, 10 },
                    { 3, 57, 5 },
                    { 3, 8, 8 },
                    { 3, 12, 4 },
                    { 3, 23, 6 },
                    { 3, 24, 7 },
                    { 3, 25, 6 },
                    { 3, 27, 7 },
                    { 4, 24, 4 },
                    { 3, 30, 8 },
                    { 3, 32, 10 },
                    { 3, 34, 7 },
                    { 3, 39, 7 },
                    { 3, 49, 10 },
                    { 3, 51, 5 },
                    { 3, 54, 3 },
                    { 3, 31, 8 },
                    { 6, 20, 7 },
                    { 6, 2, 9 },
                    { 6, 26, 6 },
                    { 8, 2, 3 },
                    { 8, 75, 0 },
                    { 7, 99, 8 },
                    { 7, 96, 9 },
                    { 8, 76, 1 },
                    { 7, 93, 1 },
                    { 7, 91, 5 },
                    { 7, 89, 3 },
                    { 7, 88, 4 },
                    { 7, 85, 7 },
                    { 7, 84, 9 },
                    { 7, 81, 9 },
                    { 7, 80, 8 },
                    { 7, 78, 8 },
                    { 7, 77, 1 },
                    { 8, 77, 1 },
                    { 7, 76, 2 },
                    { 7, 75, 9 },
                    { 7, 72, 9 },
                    { 7, 71, 7 },
                    { 7, 68, 3 },
                    { 7, 62, 10 },
                    { 6, 23, 3 },
                    { 7, 61, 5 },
                    { 7, 60, 6 },
                    { 7, 58, 8 },
                    { 7, 57, 7 },
                    { 7, 55, 10 },
                    { 7, 54, 5 },
                    { 8, 3, 5 },
                    { 8, 9, 4 },
                    { 8, 13, 4 },
                    { 8, 15, 5 },
                    { 8, 70, 5 },
                    { 8, 65, 6 },
                    { 8, 62, 9 },
                    { 8, 61, 8 },
                    { 8, 60, 10 },
                    { 8, 58, 6 },
                    { 8, 57, 2 },
                    { 8, 55, 8 },
                    { 8, 53, 6 },
                    { 8, 52, 8 },
                    { 8, 50, 9 },
                    { 8, 46, 6 },
                    { 8, 45, 6 },
                    { 8, 44, 2 },
                    { 7, 53, 3 },
                    { 8, 42, 10 },
                    { 8, 40, 3 },
                    { 8, 72, 0 },
                    { 8, 36, 5 },
                    { 8, 35, 4 },
                    { 8, 33, 10 },
                    { 8, 32, 8 },
                    { 8, 31, 6 },
                    { 8, 23, 1 },
                    { 8, 22, 7 },
                    { 8, 21, 10 },
                    { 8, 20, 5 },
                    { 8, 18, 2 },
                    { 8, 17, 0 },
                    { 8, 16, 1 },
                    { 8, 41, 4 },
                    { 8, 68, 10 },
                    { 7, 52, 7 },
                    { 7, 49, 5 },
                    { 7, 51, 1 },
                    { 6, 84, 4 },
                    { 6, 81, 0 },
                    { 6, 80, 3 },
                    { 6, 78, 7 },
                    { 6, 76, 10 },
                    { 6, 75, 0 },
                    { 6, 73, 4 },
                    { 6, 72, 10 },
                    { 6, 71, 4 },
                    { 6, 62, 8 },
                    { 6, 61, 9 },
                    { 6, 60, 1 },
                    { 6, 91, 7 },
                    { 6, 58, 7 },
                    { 6, 55, 4 },
                    { 6, 54, 10 },
                    { 6, 51, 7 },
                    { 6, 49, 0 },
                    { 6, 45, 8 },
                    { 6, 44, 10 },
                    { 6, 42, 2 },
                    { 6, 41, 10 },
                    { 6, 39, 10 },
                    { 6, 35, 9 },
                    { 6, 32, 6 },
                    { 6, 31, 10 },
                    { 6, 27, 0 },
                    { 6, 57, 9 },
                    { 6, 93, 9 },
                    { 6, 89, 2 },
                    { 6, 99, 3 },
                    { 7, 48, 7 },
                    { 7, 45, 0 },
                    { 7, 44, 9 },
                    { 7, 42, 4 },
                    { 7, 41, 3 },
                    { 7, 40, 0 },
                    { 7, 39, 2 },
                    { 7, 35, 9 },
                    { 7, 32, 1 },
                    { 7, 31, 4 },
                    { 7, 29, 8 },
                    { 7, 27, 2 },
                    { 7, 26, 2 },
                    { 6, 96, 9 },
                    { 7, 21, 7 },
                    { 7, 23, 0 },
                    { 7, 20, 1 },
                    { 8, 81, 9 },
                    { 7, 2, 3 },
                    { 7, 3, 9 },
                    { 7, 9, 9 },
                    { 7, 10, 4 },
                    { 7, 13, 8 },
                    { 7, 12, 6 },
                    { 7, 15, 6 },
                    { 7, 17, 9 },
                    { 8, 80, 10 },
                    { 7, 18, 2 },
                    { 7, 14, 1 },
                    { 8, 69, 8 }
                });

            migrationBuilder.InsertData(
                table: "Prices",
                columns: new[] { "Since", "ItemId", "Amount" },
                values: new object[,]
                {
                    { new DateTime(2019, 12, 22, 17, 47, 59, 648, DateTimeKind.Local).AddTicks(3116), 18, 93.7224561319326m },
                    { new DateTime(2020, 2, 3, 17, 47, 59, 648, DateTimeKind.Local).AddTicks(3163), 19, 30.4498449575388m },
                    { new DateTime(2020, 2, 21, 17, 47, 59, 648, DateTimeKind.Local).AddTicks(3209), 20, 16.4428597392714m },
                    { new DateTime(2020, 2, 25, 17, 47, 59, 648, DateTimeKind.Local).AddTicks(3255), 21, 50.6951825463656m },
                    { new DateTime(2019, 11, 26, 17, 47, 59, 648, DateTimeKind.Local).AddTicks(3015), 16, 24.3595962526089m },
                    { new DateTime(2020, 2, 27, 17, 47, 59, 648, DateTimeKind.Local).AddTicks(3068), 17, 37.1800485240203m },
                    { new DateTime(2020, 3, 4, 17, 47, 59, 648, DateTimeKind.Local).AddTicks(2617), 9, 80.4744442368273m },
                    { new DateTime(2020, 2, 2, 17, 47, 59, 645, DateTimeKind.Local).AddTicks(8781), 1, 55.6760325821471m },
                    { new DateTime(2019, 12, 1, 17, 47, 59, 648, DateTimeKind.Local).AddTicks(2937), 15, 7.92524544891214m },
                    { new DateTime(2020, 1, 9, 17, 47, 59, 648, DateTimeKind.Local).AddTicks(2891), 14, 1.4959491796307m },
                    { new DateTime(2020, 1, 23, 17, 47, 59, 648, DateTimeKind.Local).AddTicks(2149), 2, 60.1914422401187m },
                    { new DateTime(2020, 1, 31, 17, 47, 59, 648, DateTimeKind.Local).AddTicks(2521), 7, 84.1529738549856m },
                    { new DateTime(2019, 12, 18, 17, 47, 59, 648, DateTimeKind.Local).AddTicks(2845), 13, 20.1979566925196m },
                    { new DateTime(2019, 11, 28, 17, 47, 59, 648, DateTimeKind.Local).AddTicks(2320), 3, 20.116952490116m },
                    { new DateTime(2020, 2, 10, 17, 47, 59, 648, DateTimeKind.Local).AddTicks(2800), 12, 24.8890243120906m },
                    { new DateTime(2019, 12, 6, 17, 47, 59, 648, DateTimeKind.Local).AddTicks(2376), 4, 42.7058144671404m },
                    { new DateTime(2019, 12, 12, 17, 47, 59, 648, DateTimeKind.Local).AddTicks(2474), 6, 94.6736864720814m },
                    { new DateTime(2020, 2, 27, 17, 47, 59, 648, DateTimeKind.Local).AddTicks(2753), 11, 61.4764770779184m },
                    { new DateTime(2020, 2, 21, 17, 47, 59, 648, DateTimeKind.Local).AddTicks(2704), 10, 96.7373084727383m },
                    { new DateTime(2020, 3, 3, 17, 47, 59, 648, DateTimeKind.Local).AddTicks(2426), 5, 20.0312346313294m },
                    { new DateTime(2020, 2, 10, 17, 47, 59, 648, DateTimeKind.Local).AddTicks(2569), 8, 91.9633286502042m },
                    { new DateTime(2020, 1, 9, 17, 47, 59, 648, DateTimeKind.Local).AddTicks(3301), 22, 54.9694789363861m }
                });

            migrationBuilder.CreateIndex(
                name: "IX_AspNetRoleClaims_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "AspNetRoles",
                column: "NormalizedName",
                unique: true,
                filter: "[NormalizedName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserClaims_UserId",
                table: "AspNetUserClaims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserLogins_UserId",
                table: "AspNetUserLogins",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserRoles_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "AspNetUsers",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "AspNetUsers",
                column: "NormalizedUserName",
                unique: true,
                filter: "[NormalizedUserName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_ItemPurchases_PurchaseId",
                table: "ItemPurchases",
                column: "PurchaseId");

            migrationBuilder.CreateIndex(
                name: "IX_Items_Name_DeletedAt",
                table: "Items",
                columns: new[] { "Name", "DeletedAt" },
                unique: true,
                filter: "[DeletedAt] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Prices_ItemId",
                table: "Prices",
                column: "ItemId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AspNetRoleClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserLogins");

            migrationBuilder.DropTable(
                name: "AspNetUserRoles");

            migrationBuilder.DropTable(
                name: "AspNetUserTokens");

            migrationBuilder.DropTable(
                name: "ItemPurchases");

            migrationBuilder.DropTable(
                name: "Prices");

            migrationBuilder.DropTable(
                name: "AspNetRoles");

            migrationBuilder.DropTable(
                name: "AspNetUsers");

            migrationBuilder.DropTable(
                name: "Purchases");

            migrationBuilder.DropTable(
                name: "Items");
        }
    }
}
