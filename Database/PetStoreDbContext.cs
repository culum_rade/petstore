﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using PetStore.DataAccess;
using PetStore.Database;
using PetStore.Models.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PetStore.Model
{
    public class PetStoreDbContext : IdentityDbContext
    {
        public PetStoreDbContext(DbContextOptions<PetStoreDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Configure();
            modelBuilder.Seed();
        }

        public DbSet<Price> Prices { get; set; }
        public DbSet<Item> Items { get; set; }
        public DbSet<Toy> Toys { get; set; }
        public DbSet<Purchase> Purchases { get; set; }
        public DbSet<ItemPurchase> ItemPurchases { get; set; }

    }
}
