﻿using Microsoft.EntityFrameworkCore;
using PetStore.Models.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;

namespace PetStore.Database
{
    public static class DDLConfiguration
    {
        public static void Configure(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Item>()
                .HasIndex(i => new { i.Name, i.DeletedAt })
                .IsUnique();

            modelBuilder.Entity<Item>()
            .Property(i => i.DeletedAt)
            .HasDefaultValue(null);

            modelBuilder.Entity<Price>()
            .Property(p => p.Since)
            .HasDefaultValueSql("getdate()");

            modelBuilder.Entity<Price>()
                .HasKey(p => new { p.Since, p.ItemId });

            modelBuilder.Entity<ItemPurchase>()
                .HasKey(ip => new { ip.ItemId, ip.PurchaseId });

            modelBuilder.Entity<Purchase>()
            .Property(p => p.DateTime)
            .HasDefaultValueSql("getdate()");
        }
    }
}
