﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualBasic;
using PetStore.Enums;
using PetStore.Helpers;
using PetStore.Model;
using PetStore.Models.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PetStore.DataAccess
{
    public static class Seeder
    {
        public static void Seed(this ModelBuilder modelBuilder)
        {
            CryptoHelper crypto = CryptoHelper.Instance;
            Random random = new Random();
            int itemsNumber = 22;

            for (int i = 1; i <= itemsNumber; i++)
            {
                modelBuilder.Entity<Toy>().HasData(
                new Toy()
                {
                    Id = i,
                    Name = $"Pet toy {i}",
                    Description = $"Description {i}",
                    ItemUnit = (ItemUnits)random.Next(Enum.GetNames(typeof(ItemUnits)).Length),
                    InStock = random.Next(1000),
                    Image = $"Capture{i}.PNG"
                });

                modelBuilder.Entity<Price>().HasData(
                new Price()
                {
                    Since = DateTime.Now.AddDays(random.Next(100) - 300),
                    ItemId = i,
                    Amount = (decimal)(random.NextDouble() * 100.0)
                });
            }

            for (int i = 1; i < 100; i++)
            {
                modelBuilder.Entity<Purchase>().HasData(
                    new Purchase()
                    {
                        Id = i,
                        PurchaserFirstName = $"Name{i}",
                        PurchaserLatsName = $"LastName{i}",
                        PurchaserAddress = $"Address{i}",
                        PurchaserCardNumber = crypto.ComputeMD5Hash($"205{random.Next(1000)}56{random.Next(1000)}"),
                        DateTime = DateTime.Now.AddDays(random.Next(100) - 100)
                    });

                int purchaseItemsNumber = random.Next(10) + 1;
                int pivot = random.Next(itemsNumber - 12) + 1;

                for (int j = 0; j < purchaseItemsNumber; j++)
                {
                    modelBuilder.Entity<ItemPurchase>().HasData(
                    new ItemPurchase()
                    {
                        ItemId = pivot + j,
                        PurchaseId = i,
                        Quantity = random.Next(10 + 1)
                    }); ;
                }
            }
        }
    }
}
