﻿using PetStore.Model;
using PetStore.Models.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PetStore.DataAccess.Interfaces
{
    public interface IPurchaseRepository
    {
        public IEnumerable<Purchase> All();
        public IEnumerable<Purchase> Paginated(int pageSize, int pageNumber);
        public Task<Purchase> Insert(Purchase purchase);
        public Task<int> Count();
    }
}
