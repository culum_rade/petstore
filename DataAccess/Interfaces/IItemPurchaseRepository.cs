﻿using PetStore.Model;
using PetStore.Models.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PetStore.DataAccess.Interfaces
{
    public interface IItemPurchaseRepository
    {
        public Task<ItemPurchase> Insert(ItemPurchase itemPurchase);
    }
}
