﻿using Microsoft.EntityFrameworkCore;
using PetStore.Model;
using PetStore.Models.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PetStore.DataAccess.Interfaces
{
    public interface IItemRepositor<T> where T : Item
    {
        public Task<T> Insert(T item);
        public Task<T> Get(int id);
        public Task<bool> Exists(int id);
        public IEnumerable<T> All(bool includeDeleted = false);
        public IEnumerable<T> AvilableInStock();
        public IEnumerable<T> GetPage(IEnumerable<T> items, int pageSize, int pageNumber);
        public IEnumerable<T> SearchByName(IEnumerable<T> items, string searchString);
        public IEnumerable<T> GetByIds(List<int> ids);
        public Task<T> Update(T petToyChanges);
        public Task<T> DecreaseStock(int Id, int decreaseValue);
        public Task<T> Delete(int id);
        public Task<T> SoftDelete(int id);
    }
}
