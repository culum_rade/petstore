﻿using Microsoft.EntityFrameworkCore;
using PetStore.DataAccess.Interfaces;
using PetStore.Exceptions;
using PetStore.Model;
using PetStore.Models.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PetStore.DataAccess
{
    public abstract class SqlItemRepositor<T> : IItemRepositor<T> where T : Item
    {
        protected readonly PetStoreDbContext context;
        protected DbSet<T> Items { get; set; }
        public SqlItemRepositor(PetStoreDbContext context)
        {
            this.context = context;
        }

        public virtual async Task<T> Insert(T item)
        {
            await Items.AddAsync(item);
            await context.SaveChangesAsync();

            return item;
        }

        public virtual async Task<T> Get(int id)
        {
            T item = await Items.Where(i => i.Id == id && i.DeletedAt == null)
                .Include(i => i.Prices).FirstOrDefaultAsync();

            return item;
        }

        public virtual async Task<bool> Exists(int id)
        {
            T item = await Items.FindAsync(id);

            return item != null;
        }

        public virtual IEnumerable<T> All(bool includeDeleted = false)
        {
            return Items.Where(i => includeDeleted ? i.DeletedAt != null : i.DeletedAt == null)
                .Include(i => i.Prices);
        }

        public virtual IEnumerable<T> GetPage(IEnumerable<T> items, int pageSize, int pageNumber)
        {
            int skip = pageSize * (pageNumber - 1);

            return items.Where(i => i.DeletedAt == null).Skip(skip).Take(pageSize);
        }
        public virtual IEnumerable<T> AvilableInStock()
        {
            return Items.Where(i => i.InStock > 0 && i.DeletedAt == null)
                .Include(i => i.Prices);
        }

        public virtual IEnumerable<T> SearchByName(IEnumerable<T> items, string searchString)
        {
            return items.Where(i => i.Name.ToLower().Contains(searchString.ToLower()) && i.DeletedAt == null);
        }
        public virtual IEnumerable<T> GetByIds(List<int> ids)
        {
            return Items.Where(item => ids.Contains(item.Id) && item.DeletedAt == null)
                .Include(i => i.Prices);
        }

        public virtual async Task<T> Update(T itemChanges)
        {
            context.Items.Attach(itemChanges);
            context.Entry(itemChanges).State = EntityState.Modified;
            await context.SaveChangesAsync();

            return itemChanges;
        }

        public virtual async Task<T> DecreaseStock(int Id, int decreaseValue)
        {
            T item = await Get(Id);

            if (item.InStock < decreaseValue)
            {
                string message = String.Format("There is not enough {0} in stock, {1} available.", item.Name, item.InStock);

                throw new ThereIsNotEnoughItemsException(message);
            }

            item.InStock -= decreaseValue;
            T updatedItem = await Update(item);

            return updatedItem;
        }

        public virtual async Task<T> Delete(int id)
        {
            T item = await Items.FindAsync(id);

            if (item != null)
            {
                Items.Remove(item);
                await context.SaveChangesAsync();
            }

            return item;
        }

        public virtual async Task<T> SoftDelete(int id)
        {
            T item = await Items.FindAsync(id);
            item.DeletedAt = DateTime.Now;

            Items.Attach(item);
            context.Entry(item).Property(i => i.DeletedAt).IsModified = true;
            await context.SaveChangesAsync();

            return item;
        }
    }
}
