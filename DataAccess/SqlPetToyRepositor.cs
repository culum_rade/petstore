﻿using PetStore.Model;
using PetStore.Models.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PetStore.DataAccess
{
    public class SqlPetToyRepositor : SqlItemRepositor<Toy>
    {
        public SqlPetToyRepositor(PetStoreDbContext context) : base(context)
        {
            Items = context.Toys;
        }
    }
}
