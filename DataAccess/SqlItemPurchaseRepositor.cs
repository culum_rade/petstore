﻿using Microsoft.EntityFrameworkCore;
using PetStore.DataAccess.Interfaces;
using PetStore.Model;
using PetStore.Models.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PetStore.DataAccess
{
    public class SqlItemPurchaseRepositor : IItemPurchaseRepository
    {
        private readonly PetStoreDbContext context;
        public SqlItemPurchaseRepositor(PetStoreDbContext context)
        {
            this.context = context;
        }
        public async Task<ItemPurchase> Insert(ItemPurchase itemPurchase)
        {
            await context.ItemPurchases.AddAsync(itemPurchase);
            await context.SaveChangesAsync();

            return itemPurchase;
        }
    }
}
