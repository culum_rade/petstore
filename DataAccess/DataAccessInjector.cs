﻿using Microsoft.Extensions.DependencyInjection;
using PetStore.DataAccess.Interfaces;
using PetStore.Models.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PetStore.DataAccess
{
    public static class DataAccessInjector
    {
        public static void AddDataAccesRepositories(this IServiceCollection services)
        {
            services.AddScoped<IItemRepositor<Toy>, SqlPetToyRepositor>();
            services.AddScoped<IPurchaseRepository, SqlPurchaseRepositor>();
            services.AddScoped<IItemPurchaseRepository, SqlItemPurchaseRepositor>();
        }
    }
}
