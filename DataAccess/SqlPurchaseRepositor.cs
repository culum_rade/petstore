﻿using Microsoft.EntityFrameworkCore;
using PetStore.DataAccess.Interfaces;
using PetStore.Model;
using PetStore.Models.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PetStore.DataAccess
{
    public class SqlPurchaseRepositor : IPurchaseRepository
    {
        private readonly PetStoreDbContext context;
        public SqlPurchaseRepositor(PetStoreDbContext context)
        {
            this.context = context;
        }

        public IEnumerable<Purchase> All()
        {
            IEnumerable<Purchase> purchases = context.Purchases
                .Include(p => p.ItemPurchases)
                    .ThenInclude(ip => ip.Item)
                    .ThenInclude(i => i.Prices)
                    .OrderBy(p => p.DateTime);

            return purchases;
        }

        public async Task<int> Count()
        {
            return await context.Purchases.CountAsync();
        }

        public IEnumerable<Purchase> Paginated(int pageSize, int pageNumber)
        {
            int skip = pageSize * (pageNumber - 1);

            return context.Purchases
                .OrderByDescending(p => p.DateTime)
                .Skip(skip).Take(pageSize)
                .Include(p => p.ItemPurchases)
                    .ThenInclude(ip => ip.Item)
                        .ThenInclude(i => i.Prices);


        }
        public async Task<Purchase> Insert(Purchase purchase)
        {
            await context.Purchases.AddAsync(purchase);
            await context.SaveChangesAsync();

            return purchase;
        }
    }
}
